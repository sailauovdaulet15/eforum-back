<?php
$baseUrlFront = env('APP_URL_FRONT', 'https://eforum-front.rocketfirm.net');
$baseUrl = env('APP_URL', 'https://eforum.rocketfirm.net');
$speaker = \App\Models\Speaker::findOrFail($data->sender_id);
$user = \App\Models\User::where('speaker_id', '=', $speaker->sender_id)->first();
$isGmail = stripos($user->email, 'gmail.com');

?>
    <!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot Password</title>
</head>
<style>
    h1 {
        font-family: Verdana, Geneva, sans-serif;
    }

    h3, p, span {
        font-family: Arial, Helvetica, sans-serif;
    }
</style>
<body style="margin: 0; padding: 0">
<table
    width="100%"
    height="100%"
    border="0"
    cellpadding="0"
    cellspacing="0"
    background="#ffffff"
    style="max-width: 600px;"
>
    <tr>
        <td style="padding: 70px 50px; background: #004D94">
            <h1 style="margin: 0; font-size: 60px; line-height: 70px; color: #ffffff; font-weight: 400">
                Уведомление о сообщении
            </h1>
        </td>
    </tr>
    <tr>
        <td style="padding: 90px 50px 60px; background: #ffffff">
            <p style="margin: 0 0 50px; font-size: 22px; line-height: 36px; font-weight: 400">
                Спикер {{$speaker->full_name}} отправил вам сообщение. <br /> {{$data->text}} <br/>
                Вы можете ответить на это сообщение, авторизовавшись на сайте
                <span style="display: block; margin: 0; font-size: 18px; line-height: 36px; font-weight: 400">
</span>
            </p>
    </tr>
    <tr>
        <td style="padding: 60px; background: #eeeeee">
            <div style="text-align: center;">
                <div style="margin-bottom: 30px;">
                    <a href="https://web.facebook.com/eforumnet?_rdc=1&_rdr " style="display: inline-block; margin-right: 30px;">
                        @if(!$isGmail)
                            <img src="{{ $baseUrl.'/images/fb.png' }}" alt="facebook" style="display: block">
                        @else
                            <img src="{{ $baseUrl.'/images/fb.png' }}" alt="facebook" title="facebook"
                                 style="display: block" width="32" height="32">
                        @endif
                    </a>
                    <a href="https://www.youtube.com/channel/UCIn6tX5xFLjYaxsNKf8lDCw" style="display: inline-block; margin-right: 30px;">
                        @if(!$isGmail)
                            <img src="{{ $baseUrl.'/images/youtube.png' }}" alt="youtube" style="display: block">
                        @else
                            <img src="{{ $baseUrl.'/images/youtube.png' }}" alt="youtube" title="youtube"
                                 style="display: block" width="32" height="32">
                        @endif
                    </a>
                    <a href="https://www.instagram.com/eforumnet/" style="display: inline-block">
                        @if(!$isGmail)
                            <img src="{{ $baseUrl.'/images/instagram.png' }}" alt="instagram" style="display: block">
                        @else
                            <img src="{{ $baseUrl.'/images/instagram.png' }}" alt="instagram" title="instagram" style="display: block"
                                 width="32" height="32">
                        @endif
                    </a>
                </div>
                <p style="margin: 0; font-size: 12px; line-height: 24px; font-weight: 400; color: #999999">
                    ® 2022 E-FORUM.NET Все права защищены
                </p>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
