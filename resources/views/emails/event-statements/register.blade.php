<?php
$baseUrlFront = env('APP_URL_FRONT', 'https://eforum-front.rocketfirm.net');
$baseUrl = env('APP_URL', 'https://eforum.rocketfirm.net');

$isGmail = stripos($data->email, 'gmail.com');
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<style>
    h1 {
        font-family: Verdana, Geneva, sans-serif;
    }
    h3, p, span {
        font-family: Arial, Helvetica, sans-serif;
    }
</style>
<body style="margin: 0; padding: 0">
    <table
        width="100%"
        height="100%"
        border="0"
        cellpadding="0"
        cellspacing="0"
        background="#ffffff"
        style="max-width: 600px;"
    >
        <tr>
            <td style="padding: 70px 50px; background: #004D94">
                <div style="margin-bottom: 70px">
                    @if(!$isGmail)
                        <img src="{{ $baseUrl.'/images/logo.png' }}" alt="logo" style="display: block">
                    @else
                        <img src="{{ $baseUrl.'/images/logo.png' }}" alt="logo" title="logo" style="display: block" width="250" height="49">
                    @endif
                </div>

                <h1 style="margin: 0; font-size: 60px; line-height: 70px; color: #ffffff; font-weight: 400">
                    Спасибо за<br />
                    регистрацию!
                </h1>
            </td>
        </tr>
        <tr>
            <td style="padding: 90px 50px 60px; background: #ffffff">
                <p style="margin: 0 0 50px; font-size: 22px; line-height: 36px; font-weight: 400">
                    Добрый день, {{ $data->surname}}!
                </p>

                <span style="display: block; margin: 0; font-size: 18px; line-height: 36px; font-weight: 400">
                    Вы зарегистрировались на мероприятие:
                </span>
                <h3 style="margin: 0 0 50px; font-size: 36px; line-height: 48px; font-weight: 400">
                    {{ $data->event->title }}
                </h3>

                <span style="display: block; margin: 0; font-size: 18px; line-height: 36px; font-weight: 400">
                    Которое состоится:
                </span>
                <h3 style="margin: 0; font-size: 36px; line-height: 48px; font-weight: 400">
                    {{ \Jenssegers\Date\Date::parse($data->event->date_start)->format('j F Y')}}
                </h3>
                <span style="display: block; margin: 0 0 50px; font-size: 18px; line-height: 36px; font-weight: 400">
                    в формате {{ $data->getEventType()}}
                </span>
                @if(isset($data->event->slug))
                    <span style="display: block; margin: 0; font-size: 18px; line-height: 36px; font-weight: 400">
                        Для перехода на мероприятие,
                        <a href="{{ $baseUrlFront.'/events/'.$data->event->slug}}" style="text-decoration: none; color: #004D94;">перейдите по ссылке</a>
                    </span>
                @endif
            </td>
        </tr>
        <tr>
            <td style="padding: 60px; background: #eeeeee">
                <div style="text-align: center;">
                    <div style="margin-bottom: 30px;">
                        <a href="https://web.facebook.com/eforumnet?_rdc=1&_rdr " style="display: inline-block; margin-right: 30px;">
                            @if(!$isGmail)
                                <img src="{{ $baseUrl.'/images/fb.png' }}" alt="facebook" style="display: block">
                            @else
                                <img src="{{ $baseUrl.'/images/fb.png' }}" alt="facebook" title="facebook" style="display: block" width="32" height="32">
                            @endif
                        </a>
                        <a href="https://www.youtube.com/channel/UCIn6tX5xFLjYaxsNKf8lDCw" style="display: inline-block; margin-right: 30px;">
                            @if(!$isGmail)
                                <img src="{{ $baseUrl.'/images/youtube.png' }}" alt="youtube" style="display: block">
                            @else
                                <img src="{{ $baseUrl.'/images/youtube.png' }}" alt="youtube" title="youtube"
                                     style="display: block" width="32" height="32">
                            @endif
                        </a>
                        <a href="https://www.instagram.com/eforumnet/" style="display: inline-block">
                            @if(!$isGmail)
                                <img src="{{ $baseUrl.'/images/instagram.png' }}" alt="instagram" style="display: block">
                            @else
                                <img src="{{ $baseUrl.'/images/instagram.png' }}" alt="instagram" title="instagram" style="display: block"
                                     width="32" height="32">
                            @endif
                        </a>
                    </div>
                    <p style="margin: 0; font-size: 12px; line-height: 24px; font-weight: 400; color: #999999">
                        ® 2022 E-FORUM.NET Все права защищены
                    </p>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
