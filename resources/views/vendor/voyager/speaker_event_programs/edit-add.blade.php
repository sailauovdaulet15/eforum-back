@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <!-- form start -->
        <form role="form"
              id="form-edit-add"
              class="form-edit-add"
              action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
              method="POST" enctype="multipart/form-data">

            <div class="row">
                <div class="col-md-8">

                    <div class="panel panel-bordered">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                                <div class="form-group col-md-12">
                                    <!-- ### DETAILS ### -->
                                    <div class="panel panel panel-bordered panel-warning">
                                        <div class="panel-body">
                                            <div class="form-group" id="blockCategorySelectSpeaker">
                                                <label for="status">Фио спикера</label>
                                                <select class="form-control" name="categorySelectSpeaker">
                                                    <option value="" selected="selected"></option>
                                                </select>
                                            </div>
                                            <div class="form-group" id="blockSubCategorySelectAbout">
                                                <label for="status">О спикере</label>
                                                <select class="form-control" name="subCategorySelectAbout">
                                                    <option value="" selected="selected"></option>
                                                </select>
                                            </div>
                                            <input id="aboutSpeakerInput" type="text"
                                                   style="display: none;"
                                                   value="" name="about_speaker">
                                            <input id="speakerIdInput" type="text"
                                                   style="display: none;"
                                                   value="" name="speaker_id">
                                        </div>
                                    </div>
                                </div>
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp
                            @include('voyager::_base._base', ['getAll' => true, 'except'=> [
                                                            'speaker_id_relationship',
                                                            'about_speaker',
                                                            'event_program_id_relationship',
                                                            'event_id_relationship',
                                                    ], 'dataTypeRows' => $dataTypeRows, 'dataTypeContent' => $dataTypeContent, 'dataType' => $dataType, 'edit' => $edit, 'errors' => $errors])

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- ### DETAILS ### -->
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-body">
                            <div class="form-group" id="blockCategorySelect">
                                <label for="status">Мероприятие</label>
                                <select class="form-control" name="categorySelect">
                                    <option value="" selected="selected"></option>
                                </select>
                            </div>
                            <div class="form-group" id="blockSubCategorySelect">
                                <label for="status">Название сесии</label>
                                <select class="form-control" name="subCategorySelect">
                                    <option value="" selected="selected"></option>
                                </select>
                            </div>
                            <input id="eventProgramIdInput" type="text"
                                   style="display: none;"
                                   value="" name="event_program_id">
                            <input id="eventIdInput" type="text"
                                   style="display: none;"
                                   value="" name="event_id">
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
              enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
            <input name="image" id="upload_file" type="file"
                   onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
            {{ csrf_field() }}
        </form>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
    <input style="display: none;" value="{{App\Models\Event::getEventProgram()}}" id="categoryList"/>
    <input style="display: none;" value="{{App\Models\Speaker::getAboutSpeaker()}}" id="categoryListSpeaker"/>

@stop

@section('javascript')
    <script>
        var params = {};
        var $file;
        let categories;
        let currentCategory;
        let currentSubcategory;
        let categoriesSpeaker;
        let currentCategorySpeaker;
        let currentSubcategorySpeaker;

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: ['YYYY-MM-DD']
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();

            categories = JSON.parse($('#categoryList').val());
            checkCurrentSubcategory();
            initCategoryList();
            categoriesSpeaker = JSON.parse($('#categoryListSpeaker').val());
            checkCurrentSubcategorySpeaker();
            initCategoryListSpeaker();
        });

        //Утсановка переменных currentCategory/currentSubcategory
        function checkCurrentSubcategory()
        {
            let parentId = {{$dataTypeContent->event_program_id ? $dataTypeContent->event_program_id : 'null' }};
            let eventId = {{$dataTypeContent->event_id ? $dataTypeContent->event_id : 'null' }};

            if (eventId) {
                currentCategory = eventId;
            } else {
                currentCategory = null;
            }
            if (parentId) {
                currentSubcategory = parentId;
            } else {
                currentSubcategory = null;
            }
        }

        function initCategoryList()
        {
            let select = $('#blockCategorySelect').find('select[name="categorySelect"]');
            select = select.html("");

            for (let i = 0; i < categories.length; i++) {
                let id = categories[i]["id"],
                    name = categories[i]["name"];
                // if(!currentCategory){
                //     currentCategory = id;
                // }
                if (currentCategory && id == currentCategory) {
                    select.append($("<option>", {value: id, text: name})).prop('selected', true);
                    select.val(currentCategory);
                    initSubCategoryList(id);
                    continue;
                }
                else if(i === 0){
                    initSubCategoryList(id);
                }
                select.append($("<option>", {value: id, text: name}));
            }
        }

        function initSubCategoryList()
        {
            let select = $('#blockSubCategorySelect').find('select[name="subCategorySelect"]');
            select = select.html("");
            changeSubcategoryInputVal(null);

            for (let i = 0; i < categories.length; i++) {//категории
                let id = categories[i]["id"];
                if (id == currentCategory) {
                    if (categories[i]['children'].length === 1) {
                        changeCategoryInputVal(currentCategory);
                    }
                    for (let j = 0; j < categories[i]['children'].length; j++) {//подкатегории
                        let id = categories[i]['children'][j]["id"],
                            name = categories[i]['children'][j]["name"];

                        if (currentSubcategory && id == currentSubcategory) {
                            select.append($("<option>", {value: id, text: name}));
                            select.val(currentSubcategory);
                            changeSubcategoryInputVal(id);
                            continue;
                        }
                        else if(j === 0){
                            changeCategoryInputVal(currentCategory);
                        }
                        select.append($("<option>", {value: id, text: name}));
                    }
                }
            }
        }

        function changeSubcategoryInputVal(id)
        {
            $('#eventProgramIdInput').val(id);
        }
        function changeCategoryInputVal(eventId)
        {
            $('#eventIdInput').val(eventId);
        }

        function checkCurrentSubcategorySpeaker() {
            let parentId = "{{$dataTypeContent->about_speaker ? $dataTypeContent->about_speaker : null }}";
            let eventId = {{$dataTypeContent->speaker_id ? $dataTypeContent->speaker_id : 'null' }};
            if (eventId) {
                currentCategorySpeaker = eventId;
            } else {
                currentCategorySpeaker = null;
            }
            if (parentId) {
                currentSubcategorySpeaker = parentId;
            } else {
                currentSubcategorySpeaker = null;
            }
        }

        function initCategoryListSpeaker() {
            let select = $('#blockCategorySelectSpeaker').find('select[name="categorySelectSpeaker"]');
            select = select.html("");
            for (let i = 0; i < categoriesSpeaker.length; i++) {
                let id = categoriesSpeaker[i]["id"],
                    name = categoriesSpeaker[i]["name"];
                // if(!currentCategory){
                //     currentCategory = id;
                // }
                if (currentCategorySpeaker && id == currentCategorySpeaker) {
                    select.append($("<option>", {value: id, text: name})).prop('selected', true);
                    select.val(currentCategorySpeaker);
                    initSubCategoryListSpeaker(id);
                    continue;
                } else if (i === 0) {
                    initSubCategoryListSpeaker(id);
                }
                select.append($("<option>", {value: id, text: name}));
            }
        }

        function initSubCategoryListSpeaker() {
            let select = $('#blockSubCategorySelectAbout').find('select[name="subCategorySelectAbout"]');
            select = select.html("");
            changeSubcategoryInputValSpeaker(null);

            for (let i = 0; i < categoriesSpeaker.length; i++) {//категории
                let id = categoriesSpeaker[i]["id"];
                if (id == currentCategorySpeaker) {
                    if (categoriesSpeaker[i]['children'].length === 1) {
                        changeCategoryInputValSpeaker(currentCategorySpeaker);
                    }
                    for (let j = 0; j < categoriesSpeaker[i]['children'].length; j++) {//подкатегории
                        let id = categoriesSpeaker[i]['children'][j]["id"],
                            name = categoriesSpeaker[i]['children'][j]["name"];

                        if (currentSubcategorySpeaker && id == currentSubcategorySpeaker) {
                            select.append($("<option>", {value: id, text: name}));
                            select.val(currentSubcategorySpeaker);
                            changeSubcategoryInputValSpeaker(name);
                            continue;
                        } else if (j === 0) {
                            changeCategoryInputValSpeaker(currentCategorySpeaker);
                        }
                        select.append($("<option>", {value: id, text: name}));
                    }
                }
            }
        }

        function changeSubcategoryInputValSpeaker(name) {
            $('#aboutSpeakerInput').val(name);
        }

        function changeCategoryInputValSpeaker(eventId) {
            $('#speakerIdInput').val(eventId);
        }

        $('#blockCategorySelectSpeaker').find('select[name="categorySelectSpeaker"]').change(function () {
            let id = $(this).val();
            currentCategorySpeaker = id;
            currentSubcategorySpeaker = null;
            initSubCategoryListSpeaker(id);
            changeCategoryInputValSpeaker(id);
            changeSubcategoryInputValSpeaker(null)
        });
        $('#blockSubCategorySelectAbout').find('select[name="subCategorySelectAbout"]').change(function () {
            let id = $(this).val();
            let name = $(this).val();
            currentSubcategorySpeaker = id;
            initSubCategoryListSpeaker(id);
            changeSubcategoryInputValSpeaker(name);
        });

        $('#blockCategorySelect').find('select[name="categorySelect"]').change(function () {
            let id = $(this).val();
            currentCategory = id;
            currentSubcategory = null;
            initSubCategoryList(id);
            changeCategoryInputVal(id);
            changeSubcategoryInputVal(null)
        });
        $('#blockSubCategorySelect').find('select[name="subCategorySelect"]').change(function () {
            let id = $(this).val();
            currentSubcategory = id;
            initSubCategoryList(id);
            changeSubcategoryInputVal(id);
        });

        $('#form-edit-add').submit(function (event) {
            event.preventDefault();
            id = $('#blockCategorySelect').find('select[name="categorySelect"]');
            sId = $('#blockSubCategorySelect').find('select[name="subCategorySelect"]');
            ids = $('#blockCategorySelectSpeaker').find('select[name="categorySelectSpeaker"]');
            sIds = $('#blockSubCategorySelectAbout').find('select[name="subCategorySelectAbout"]');

            if (id.val() == 0) {
                id.val(null);
                changeCategoryInputVal(null);
            }
            if (sId.val() == 0) {
                sId.val(null);
                changeSubcategoryInputVal(null);
            }
            if (ids.val() == 0) {
                ids.val(null);
                changeCategoryInputValSpeaker(null);
            }
            if (sIds.val() == 0) {
                sIds.val(null);
                changeSubcategoryInputValSpeaker(null);
            }

            $(this).unbind('submit').submit();
        })
    </script>
@stop
