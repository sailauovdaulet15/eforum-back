<?php

namespace App\Mail;

use App\Models\EventStatement;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EventStatements extends Mailable
{
    use Queueable, SerializesModels;

    public $eventStatement;

    public function __construct(EventStatement $eventStatement)
    {
        $this->eventStatement = $eventStatement;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Регистрация на мероприятие на сайте E-forum.net')
            ->view('emails.event-statements.register', ['data' => $this->eventStatement]);
    }
}
