<?php


namespace App\Mail;


use App\Models\EventStatement;
use App\Models\SpeakerMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SpeakerMessagesMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $speakerMessage;

    public function __construct(SpeakerMessage $speakerMessage)
    {
        $this->speakerMessage = $speakerMessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Уведомление о сообщении')
            ->view('emails.speaker-messages.messages', ['data' => $this->speakerMessage]);

    }
}
