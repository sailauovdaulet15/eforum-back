<?php

namespace App\Mail;

use App\Models\EventStatement;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $token;

    public $email;

    public function __construct($token, $email)
    {
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Изменение пароля на сайте E-Forum.net')
            ->view('emails.password.reset-password', ['token' => $this->token, $this->email] );
    }
}
