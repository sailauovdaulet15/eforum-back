<?php

namespace App\Observers;

use App\Mail\SpeakerMessagesMailable;
use App\Models\Speaker;
use App\Models\SpeakerMessage;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class SpeakerMessagesObserver
{
    /**
     * Handle the SpeakerMessage "created" event.
     *
     * @param \App\Models\SpeakerMessage $speakerMessage
     * @return void
     */
    public function creating(SpeakerMessage $speakerMessage)
    {
        $speaker = Speaker::findOrFail($speakerMessage->recipient_id);
        if ($speakerMessage->parent_id != null) {
            $speaker = Speaker::findOrFail($speakerMessage->sender_id);
        }
        $user = User::where('speaker_id', '=', $speaker->id)->first();
        if ($user->email) {
            Mail::to($user->email)->send(new SpeakerMessagesMailable($speakerMessage));
        }
    }
}
