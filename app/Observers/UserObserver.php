<?php

namespace App\Observers;

use App\Models\Speaker;
use App\Models\User;

class UserObserver
{
    public function deleted(User $user){
      Speaker::findOrFail($user->speaker_id)->delete();
    }
}
