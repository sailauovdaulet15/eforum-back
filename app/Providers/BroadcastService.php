<?php


namespace App\Providers;


use App\Models\Broadcast;
use App\Models\EventStatement;

class BroadcastService
{
    public function getUserBroadcast()
    {
        $user = $this->getAuthUser();
        $eventStatements = EventStatement::where('user_id', '=', $user->id)->get()->pluck('event_id')->toArray();
        $broadcasts = Broadcast::whereIn('event_id', $eventStatements)->orderBy('id','desc')->paginate(6);
        return $broadcasts;
    }
    private function getAuthUser()
    {
       return auth('api')->user();
    }
}
