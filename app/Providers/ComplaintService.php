<?php


namespace App\Providers;


use App\Models\Complaint;
use Illuminate\Database\Eloquent\Model;

class ComplaintService
{
    public function store(array $data):Model
    {
        return Complaint::query()->create(array_merge($data, $this->setRequestWithToken()));
    }

    public function setRequestWithToken():array
    {
        if ( $user = auth('api')->user() ) {
            return ['user_id' => $user->id];
        }
        return  [];
    }
}
