<?php


namespace App\Providers;



use App\Models\Comment;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CommentService
{
    public function store(array $data):Model
    {
        $event = Event::where('slug', $data['event_slug'])->select('id')->first();
        $data['event_id'] = $event->id;
        return Comment::query()->create(array_merge($data, $this->setRequestWithToken()));
    }

    public function setRequestWithToken():array
    {
        if ( $user = auth('api')->user() ) {
            return ['user_id' => $user->id];
        }
        return  [];
    }

    public function setArray(){
        $user = auth('api')->user();
        $comments = Comment::where('email', $user->email)->orderBy('id', 'desc')->get();
        $result = [];
        foreach ($comments as $comment){
            $data = [
                'id' => $comment->id ,
                'event_name' => $comment->event->title,
                'event_slug' => $comment->event->slug,
                'content' => $comment->content,
                'date' => Carbon::create($comment->created_at)->timestamp
            ];
            array_push($result, $data );
        }
        return $result;
    }
}
