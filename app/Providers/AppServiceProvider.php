<?php

namespace App\Providers;

use App\Voyager\FormFields\LinkFormField;
use App\Voyager\FormFields\JsonOneFieldOptionFormField;
use App\Voyager\FormFields\JsonTwoFieldOptionFormField;
use App\Voyager\FormFields\PositionFormField;
use App\Voyager\FormFields\SelectDependentDropdown;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Date\Date;
use MonstreX\VoyagerExtension\Controllers\VoyagerExtensionBaseController;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addFormField(JsonOneFieldOptionFormField::class);
        Voyager::addFormField(JsonTwoFieldOptionFormField::class);
//        Voyager::addFormField(PositionFormField::class);
        Voyager::addFormField(LinkFormField::class);
        Voyager::addFormField(SelectDependentDropdown::class);

        $this->app->bind(
            VoyagerExtensionBaseController::class,
            \App\Http\Controllers\VoyagerExtension\VoyagerExtensionBaseController::class
        );
//
//        $this->app->bind(
//            'TCG\Voyager\Http\Controllers\VoyagerBaseController',
//            'App\Voyager\Controllers\JsonTwoFieldOptionController'
//        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Date::setlocale(config('app.locale'));
    }
}
