<?php


namespace App\Providers;


use App\Models\Comment;
use App\Models\Event;
use App\Models\EventStatement;
use Illuminate\Database\Eloquent\Model;

class EventStatementService
{
    public function store(array $data):Model
    {
        return EventStatement::query()->create(array_merge($data, $this->setRequestWithToken()));
    }

    public function setRequestWithToken():array
    {

        if ( $user = auth('api')->user() ) {
            return ['surname' => $user->full_name, 'phone' => $user->phone ,'email' => $user->email,
                'user_id' => $user->id];
        }
        return  [];
    }
}
