<?php

namespace App\Console\Commands;

use App\Models\LocationCity;
use App\Models\LocationCountry;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use TCG\Voyager\Facades\Voyager;

class CreateNewAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voyager:createNewAdmin {email} {--create}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make sure there is a user with the admin role that has all of the necessary permissions.';

    /**
     * Get user options.
     */
    protected function getOptions(): array
    {
        return [
            ['create', null, InputOption::VALUE_NONE, 'Create an admin user', null],
        ];
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {

        // Get or create user
        $user = $this->getUser(
            $this->option('create')
        );
        // the user not returned
        if (!$user) {
            exit;
        }

        // Get or create role
        $role = $this->getAdministratorRole();

        // Get all permissions
        $permissions = Voyager::model('Permission')->all();

        // Assign all permissions to the admin role
        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );

        // Ensure that the user is admin
        $user->role_id = $role->id;
        $user->save();

        $this->info('Админ успешно создан. '. 'У пользователя ' . $user->email . ' есть полный доступ к вашему сайту');
    }

    /**
     * Get command arguments.
     *
     * @return array
     */
    protected function getArguments(): array
    {
        return [
            ['email', InputOption::VALUE_REQUIRED, 'The email of the user.', null],
        ];
    }

    /**
     * Get the administrator role, create it if it does not exists.
     *
     * @return mixed
     */
    protected function getAdministratorRole()
    {
        $role = Voyager::model('Role')->firstOrNew([
            'name' => 'admin',
        ]);

        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Administrator',
            ])->save();
        }

        return $role;
    }

    /**
     * Get or create user.
     *
     * @param bool $create
     *
     * @return User
     */
    protected function getUser(bool $create = false): User
    {
        $email = $this->argument('email');

        $model = Auth::guard(app('VoyagerGuard'))->getProvider()->getModel();
        $model = Str::start($model, '\\');

        $now = Carbon::now()->format('Y-m-d H:i:s');

        $city = LocationCity::firstOrFail();


        // If we need to create a new user go ahead and create it
        if ($create) {
            $password = $this->secret('Введите пароль админа');
            $phone = $this->askValid('Введите номер телефона',
                'phone', ['required', 'digits:10']);

            // Ask for email if there wasnt set one
            if (!$email) {
                $email = $this->ask('Введите email админа');
            }

            // check if user with given email exists

            if ($model::where('email', $email)->exists()) {
                $this->info("Не удалось создать пользователя " . $email . ' уже существует.');

            }


            return call_user_func($model . '::forceCreate', [
                'full_name' => 'Admin',
                'email' => $email,
                'password' => Hash::make($password),
                'phone' => $phone,
                'company' => env('APP_NAME'),
                'post' => 'admin',
                'location_country_id' => $city->location_country_id,
                'location_city_id' => $city->id,
                'date_time_last_login' => $now,
                'account_is_active' => true,
            ]);
        }


        return call_user_func($model . '::where', 'email', $email)->firstOrFail();
    }

    protected function askValid($question, $field, $rules)
    {
        $value = $this->ask($question);

        if ($message = $this->validateInput($rules, $field, $value)) {
            $this->error($message);

            return $this->askValid($question, $field, $rules);
        }

        return $value;
    }


    protected function validateInput($rules, $fieldName, $value): ?string
    {
        $validator = Validator::make([
            $fieldName => $value
        ], [
            $fieldName => $rules
        ]);

        return $validator->fails()
            ? $validator->errors()->first($fieldName)
            : null;
    }
}
