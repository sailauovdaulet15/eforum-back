<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\EventProgram;
use App\Models\OrganizerEvent;

class FixPositionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'position:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $models = EventProgram::where(['deleted_at' => null])->get();
            $arr = [];
            foreach ($models as $model){
                $arr[$model->event_id][] = $model->id;
            }
            foreach ($arr as $category) {
                $pos = 1;
                foreach ($category as $item) {
                    EventProgram::where(['id' => $item])->update(['position' => $pos]);
                    $pos ++;
                }
            }

            $models = OrganizerEvent::where(['deleted_at' => null])->get();
            $arr = [];
            foreach ($models as $model) {
                $arr[$model->event_id][] = $model->id;
            }
            foreach ($arr as $category) {
                $pos = 1;
                foreach ($category as $item) {
                    OrganizerEvent::where(['id' => $item])->update(['position' => $pos]);
                    $pos ++;
                }
            }
        }catch (\Exception $ex){
            var_dump($ex->getMessage());
        }
        var_dump('success');
        return Command::SUCCESS;
    }

    private function fix($models)
    {
        $arr = [];
        foreach ($models as $model){
            $arr[$model->event_id][] = $model;
        }

        foreach ($arr as $category){
            $pos = 0;
            foreach ($category as $item){
                $pos+=1;
                $item->update(['position'=>$pos]);
            }
        }
    }
}
