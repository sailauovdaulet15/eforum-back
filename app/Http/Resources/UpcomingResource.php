<?php

namespace App\Http\Resources;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UpcomingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var Event $this */
        $now = Carbon::now()->format('Y-m-d');
        return [
            'format'  => 'До мероприятия осталось '.Carbon::parse($this->date_start)->diffInDays($now).' дней',
        ];
    }
}
