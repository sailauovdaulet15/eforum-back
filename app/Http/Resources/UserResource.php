<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var User $this */
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'email' => $this->email,
            'location_country_id' => $this->locationCountry->name,
            'location_city_id' => $this->locationCity->name,
            'phone' => $this->phone,
            'avatar' => $this->getAvatar(),
            'company' => $this->company,
            'post' => $this->post,
            'is_notification_events' => $this->is_notification_events,
            'isSpeaker' => $this->getSpeaker(),
        ];
    }
}
