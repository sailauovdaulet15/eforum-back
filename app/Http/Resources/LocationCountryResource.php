<?php

namespace App\Http\Resources;

use App\Models\LocationCountry;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LocationCountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var LocationCountry $this */
        return [
            'id' => $this->id,
            'name'  => $this->name,
        ];
    }
}
