<?php

namespace App\Http\Resources;

use App\Models\SpeakerEvent;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class SpeakerEventsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var SpeakerEvent $this */
        return [
            'id' => $this->speaker->id ?? null,
            'full_name' => $this->speaker->full_name ?? null,
            'about_speaker' => $this->about_speaker ?? null,
            'photo' => $this->getPhoto(),
            'moderator' => $this->is_moderator ?? null,
        ];
    }
}
