<?php

namespace App\Http\Resources;

use App\Models\HandbookEventStatus;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HandbookEventStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookEventStatus $this */
        return [
            'id' => $this->id,
            'name'  => $this->name,
        ];
    }
}
