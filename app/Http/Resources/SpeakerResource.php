<?php

namespace App\Http\Resources;

use App\Models\Speaker;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SpeakerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Speaker $this */
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'about_speaker' => Speaker::changeJson($this->about_speaker) ?? '',
        ];
    }
}
