<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BroadcastUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'event_title' => $this->event->title,
            'hall' => $this->handbookHall ? new HandbookHallResource($this->handbookHall) : [],
            'link' => $this->link_broadcast,
            'date' => Carbon::create($this->date)->timestamp,
        ];
    }
}
