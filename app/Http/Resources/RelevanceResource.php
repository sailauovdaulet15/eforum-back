<?php

namespace App\Http\Resources;

use App\Models\HandbookEventRelevance;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RelevanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookEventRelevance $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
