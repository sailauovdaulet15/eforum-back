<?php

namespace App\Http\Resources;

use App\Models\EventComment;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var EventComment $this */
        return [
            'id' => $this->id,
            'event_id' => $this->event_id,
            'speaker_id' => $this->speaker_id,
            'comment_text' => $this->comment_text,
            'created_at' => Carbon::create($this->created_at)->format('d-m-Y'),
            'updated_at' => Carbon::create($this->updated_at)->format('d-m-Y'),
        ];
    }
}
