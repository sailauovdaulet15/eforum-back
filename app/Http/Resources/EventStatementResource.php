<?php

namespace App\Http\Resources;

use App\Models\EventStatement;
use Illuminate\Http\Resources\Json\JsonResource;

class EventStatementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var EventStatement $this */
        return [
            'surname' => $this->surname,
            'event_id' => $this->event_id,
            'phone' => $this->phone,
            'email' => $this->email,
            'post' => $this->post,
            'organization' => $this->organization,
            'location_country_id' => $this->location_country_id,
            'location_city_id' => $this->location_city_id,
        ];
    }
}
