<?php

namespace App\Http\Resources;

use App\Models\EventProgram;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProgramsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var EventProgram $this */
        return [
            'id' => $this->id,
            'time_from' => Carbon::create($this->time_from)->timestamp,
            'time_to' => Carbon::create($this->time_to)->timestamp,
            'title' => $this->title,
            'description' => $this->description,
            'position' => $this->position,
            'handbook_hall_id' => $this->handbook_hall_id,
            'speakers' => $this->speakers ? SpeakerEventProgramResource::collection($this->speakers) : [],
            'moderator' => SpeakerEventProgramResource::collection($this->moderators),
        ];
    }
}
