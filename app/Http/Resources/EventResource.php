<?php

namespace App\Http\Resources;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $eventProgram = $this->eventPrograms()->orderBy('time_from', 'asc')->first();
        $time = '';
        if($eventProgram) {
           $time = $eventProgram['time_from'];
        }

        /** @var Event $this */
        return [
            'id' => $this->id,
            'relevance' => new HandbookEventRelevanceResource($this->handbookEventRelevance),
            'report_counter' => $this->dateStart(),
            'date_from' => Carbon::create($this->date_start),
            'date_to' => Carbon::create($this->date_end),
            'time_from' => $time ? Carbon::create($time) : null,
            'title' => $this->getTranslate('title', $request),
            'slug' => $this->slug,
            'category' => new CategoryResource($this->handbookCategory),
            'handbook_event_kind' => new HandbookEventKindResource($this->handbookEventKind),
            'type' => $this->handbookEventType ? new HandbookEventTypeResource($this->handbookEventType) : null,
            'location' => $this->handbookSite ? new HandbookSiteResource($this->handbookSite) : null,
            'country' => $this->locationCountry ? new LocationCountryResource($this->locationCountry) : null,
            'city' => $this->locationCity ? new LocationCityResource($this->locationCity) : null,
            'annotation' => $this->short_description,
            'description' => str_replace('/storage/events/', ' https://eforum.rocketfirm.net/storage/events/',
                $this->description),
            'background' => isset($this->main_image) ? Storage::disk(config('voyager.storage.disk'))->url($this->main_image) : $this->getImage(),
            'organizers' => $this->organizerEvents ? OrganizerEventsResource::collection(
                $this->organizerEvents()->orderByDesc('position')->get()
            ) : null,
            'speakers' => $this->speakerEvents ? SpeakerEventsResource::collection($this->speakerEvents) : [],
            'program' => isset(json_decode($this->program)[0]->download_link) ? Storage::disk(config('voyager.storage.disk'))->url(json_decode($this->program)[0]->download_link) : null,
            'broadcast' => $this->getBroadCasts(),
            'materials' => $this->getMaterials(),
        ];
    }
}
