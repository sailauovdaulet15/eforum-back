<?php

namespace App\Http\Resources;

use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class NewsSlugResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var News $this */
        return [
            'id' => $this->id,
            'slug' => $this->seo_slug,
            'title' => $this->title,
            'annotation' => $this->annotation,
            'description' => str_replace('/storage/news/', ' https://eforum.rocketfirm.net/storage/news/',
                $this->content),
            'date_publication' => Carbon::create($this->created_at)->timestamp,
            'image_uri' => $this->getImage(),
        ];
    }
}
