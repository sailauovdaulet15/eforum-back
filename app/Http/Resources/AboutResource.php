<?php

namespace App\Http\Resources;

use App\Models\About;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AboutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var About $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'image' => Storage::disk(config('voyager.storage.disk'))->url($this->image_uri),
            'files' => Storage::disk(config('voyager.storage.disk'))->url($this->files),
            'seo_title' => $this->seo_title,
            'seo_description' => $this->seo_description,
            'seo_image' => Storage::disk(config('voyager.storage.disk'))->url($this->seo_image),
            'seo_slug' => $this->seo_slug
        ];
    }
}
