<?php

namespace App\Http\Resources;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class EventsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $eventProgram = $this->eventPrograms()->orderBy('time_from', 'asc')->first();
        $time = '';
        if($eventProgram) {
            $time = $eventProgram['time_from'];
        };
        $datetime =  $this->date_start . ' ' . $time;

        /** @var Event $this */
        return [
            'id' => $this->id,
            'title'  => $this->getTranslate('title', $request),
            'slug' => $this->slug,
            'date_from' => $time ? Carbon::create($datetime)->format('Y-m-d\TH:i:s') : Carbon::create($this->date_start)->format('Y-m-d\TH:i:s'),
            'type' => new HandbookEventTypeResource($this->handbookEventType),
            'background' => isset($this->main_image) ? Storage::disk(config('voyager.storage.disk'))->url($this->main_image) : $this->getImage(),
            'category' => new CategoryResource($this->handbookCategory),
            'position' => $this->position,
            'handbook_event_kind' => isset($this->handbookEventKind->id) ? new HandbookEventKindResource($this->handbookEventKind) :null,
        ];
    }
}
