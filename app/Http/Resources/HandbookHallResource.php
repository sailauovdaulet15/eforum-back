<?php

namespace App\Http\Resources;

use App\Models\HandbookHall;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HandbookHallResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookHall $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'city' => new LocationCityResource($this->locationCity),
            'site' => new HandbookSiteResource($this->handbookSite),
        ];
    }
}
