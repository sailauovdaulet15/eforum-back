<?php

namespace App\Http\Resources;

use App\Models\MainMenu;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MainMenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'label' => $this->label, 
            'link' => $this->link, 
        ];
    }
}