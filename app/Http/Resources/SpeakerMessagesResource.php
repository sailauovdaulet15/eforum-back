<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SpeakerMessagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

       $r = $this->type == 1 ? SpeakerRepliesResource::collection($this->childs) : new SpeakerRepliesResource($this->child);
        return [
            'id' => $this->id,
            'sender' => $this->sender->full_name,
            'recipient' => $this->recipient->full_name,
            'recipient_id' => $this->recipient->id,
            'text' => $this->text,
            'total_replies' => $this->childs->count(),
            'created_at' => Carbon::create($this->created_at)->timestamp,
            'replies' => $this->child ? $r : []
        ];
    }
}
