<?php

namespace App\Http\Resources;

use App\Models\Material;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class MaterialsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Material $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'file' => isset(json_decode($this->file)[0]->download_link) ? Storage::disk(config('voyager.storage.disk'))->url(json_decode($this->file)[0]->download_link) : null,
        ];
    }
}
