<?php

namespace App\Http\Resources;

use App\Models\HandbookTypeSponsor;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HandbookTypeSponsorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookTypeSponsor $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
