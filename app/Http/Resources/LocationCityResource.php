<?php

namespace App\Http\Resources;

use App\Models\LocationCity;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LocationCityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var LocationCity $this */
        return [
            'id' => $this->id,
            'name'  => $this->name,
            'location_country_id' => $this->location_country_id
        ];
    }
}
