<?php

namespace App\Http\Resources;

use App\Models\Contact;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'text1' => $this->text1,
            'text2' => $this->text2,
            'email' => $this->email,
            'text3' => $this->text3,
            'icon1' => empty($this->icon1) ? "" : Storage::disk(config('voyager.storage.disk'))->URL::url($this->icon1),
            'link1' => $this->link1,
            'icon2' => empty($this->icon2) ? "" : Storage::disk(config('voyager.storage.disk'))->URL::url($this->icon2),
            'link2' => $this->link2,
            'icon3' => empty($this->icon3) ? "" : Storage::disk(config('voyager.storage.disk'))->URL::url($this->icon3),
            'link3' => $this->link3,
            'icon4' => empty($this->icon4) ? "" : Storage::disk(config('voyager.storage.disk'))->URL::url($this->icon4),
            'link4' => $this->link4,
            'icon5' => empty($this->icon5) ? "" : Storage::disk(config('voyager.storage.disk'))->URL::url($this->icon5),
            'link5' => $this->link5,
        ];
    }
}
