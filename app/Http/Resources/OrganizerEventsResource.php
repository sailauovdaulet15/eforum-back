<?php

namespace App\Http\Resources;

use App\Models\OrganizerEvent;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class OrganizerEventsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var OrganizerEvent $this */
        return [
            'id' => $this->id,
            'name' => $this->organizer->name,
            'logo' => isset($this->organizer->logo) ? Storage::disk(config('voyager.storage.disk'))->url($this->organizer->logo) : $this->organizer->getLogo(),
            'type' => new HandbookTypeSponsorResource($this->handbookTypeSponsor),
            'position' => $this->position
        ];
    }
}
