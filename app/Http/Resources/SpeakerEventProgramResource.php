<?php

namespace App\Http\Resources;

use App\Models\SpeakerEventProgram;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SpeakerEventProgramResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var SpeakerEventProgram $this */
        return [
            'id' => $this->speaker->id ?? '',
            'full_name' => $this->speaker->full_name ?? '',
            'about_speaker' => $this->speaker ? $this->about_speaker : '',
            'moderator' => $this->speaker ? $this->is_moderator : '',
        ];
    }
}
