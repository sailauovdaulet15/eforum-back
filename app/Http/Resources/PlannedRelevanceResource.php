<?php

namespace App\Http\Resources;

use App\Models\Event;
use App\Models\HandbookEventRelevance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlannedRelevanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Event $this */
        $now = Carbon::now();
        $dateStart = new Carbon($this->date_start);
        return [
            'report_counter' => 'До начала мероприятия осталось '.$dateStart->diff($now)->format('%dдня  %hчасов %mминут %sсекунд'),
        ];
    }
}
