<?php

namespace App\Http\Resources;

use App\Models\Broadcast;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BroadCastsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Broadcast $this */
        return [
            'id' => $this->id,
            'hall' => new HandbookHallResource($this->handbookHall),
            'link' => $this->link_broadcast,
            'broadcast_language' => new HandbookLanguageResource($this->handbookLanguage),
            'date' => Carbon::create($this->date)->timestamp,
        ];
    }
}
