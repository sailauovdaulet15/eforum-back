<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventGoogleScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'event_id' => $this['id'] ?? '',
            'title' => $this['title'] ?? '',
            'short_description' => $this['short_description'] ?? '',
            'category' => new CategoryResource($this['handbook_category_id']) ?? '',
            'time_from' => Carbon::create($this['current_date'] . $this['min_time_from'])->format('Y-m-d\TH:i') ?? '',
            'time_to' => Carbon::create($this['current_date'] . $this['min_time_to'])->format('Y-m-d\TH:i') ?? '',
        ];
    }
}
