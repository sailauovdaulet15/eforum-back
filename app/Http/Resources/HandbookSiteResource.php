<?php

namespace App\Http\Resources;

use App\Models\HandbookSite;
use GeoJson\Exception\Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HandbookSiteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookSite $this */
        return [
            'id' => $this->id,
            'name'  => $this->name,
        ];
    }
}
