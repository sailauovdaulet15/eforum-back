<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'full_name' => $this->full_name ?? $this->user->full_name,
            'avatar' => $this->getAvatar(),
            'email' => $this->email ?? $this->user->email,
            'content' => $this->content,
            'event_id' =>  $this->event_id,
            'event_slug' => $this->event->slug ?? null,
            'parent' => ($this->parent) ? new CommentUserResource($this->parent) : [],
            'created_at' => Carbon::create($this->created_at)->timestamp,
        ];
    }
}
