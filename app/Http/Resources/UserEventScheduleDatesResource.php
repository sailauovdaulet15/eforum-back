<?php

namespace App\Http\Resources;

use App\Models\Event;
use App\Models\EventProgram;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserEventScheduleDatesResource extends JsonResource
{
    private $date;

    public function __construct($date, $collection)
    {
        parent::__construct($collection);
        $this->date = $date;
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {

        /** @var Event $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'city' => $this->locationCity->name,
            'site' => $this->handbookSite->name,
            'category' => new CategoryResource($this->handbookCategory),
            'type' => new HandbookEventTypeResource($this->handbookEventType),
            'status' => new HandbookEventStatusResource($this->handbookEventStatus),
            'relevance' => new HandbookEventRelevanceResource($this->handbookEventRelevance),
            'time_from' => $this->getTimeFrom($this->date),
            'time_to' => $this->getTimeTo($this->date),
        ];
    }
}
