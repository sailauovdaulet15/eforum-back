<?php

namespace App\Http\Resources;

use App\Models\HandbookLanguage;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HandbookLanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookLanguage $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
