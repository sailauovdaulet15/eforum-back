<?php

namespace App\Http\Resources;

use App\Models\MainPage;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;


class MainPageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var MainPage $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'video' => $this->when($this->isImage === false, $this->video),
            'image' => $this->when($this->isImage === true, Storage::disk(config('voyager.storage.disk'))->url($this->image)),
        ];
    }
}
