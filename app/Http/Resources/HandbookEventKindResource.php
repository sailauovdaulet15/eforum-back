<?php

namespace App\Http\Resources;

use App\Models\HandbookEventKind;
use App\Models\HandbookEventType;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HandbookEventKindResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookEventKind $this */
        return [
            'id' => $this->id,
            'name'  => $this->name,
        ];
    }
}
