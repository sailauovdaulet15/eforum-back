<?php

namespace App\Http\Resources;

use App\Models\Event;
use App\Models\HandbookEventRelevance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HandbookEventRelevanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookEventRelevance $this */
        return [
            'id' => $this->id,
            'name'  => $this->name,
        ];
    }
}
