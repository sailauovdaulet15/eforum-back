<?php

namespace App\Http\Resources;

use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /** @var News $this */
        return [
            'title' => $this->title,
            'slug' => $this->seo_slug,
            'annotation' => $this->annotation,
            'date_publication' => Carbon::create($this->created_at)->timestamp,
            'image_uri' => $this->getImage(),
        ];
    }
}
