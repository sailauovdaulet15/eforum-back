<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentChildResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'full_name' => $this->full_name ?? $this->user->full_name,
            'avatar' => $this->getAvatar(),
            'email' => $this->email ?? $this->user->email,
            'content' => $this->content,
            'event_id' =>  $this->event_id,
            'event_slug' => $this->event->slug ?? null,
            'user_id' => $this->user_id,
            'created_at' => Carbon::create($this->created_at)->timestamp,
            'child' => ($this->child) ? CommentChildResource::collection($this->child) : [],
        ];
    }
}
