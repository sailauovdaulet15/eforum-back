<?php

namespace App\Http\Resources;

use App\Models\HandbookEventType;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HandbookEventTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var HandbookEventType $this */
        return [
            'id' => $this->id,
            'name'  => $this->name,
            'display' => $this->display,
        ];
    }
}
