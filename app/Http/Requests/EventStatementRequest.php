<?php

namespace App\Http\Requests;

use App\Rules\ValidRecaptcha;
use Illuminate\Foundation\Http\FormRequest;

class EventStatementRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        if(auth('api')->user()){
            return [
                'surname' => 'string',
                'event_id' => 'required|integer|exists:events,id',
                'phone' => 'string',
                'post' => 'required|string',
                'organization' => 'required|string',
                'location_country_id' => 'required|integer|exists:location_countries,id',
                'location_city_id' => 'required|integer|exists:location_cities,id',
                'token' => ['required', new ValidRecaptcha]
            ];
        }
        return [
            'surname' => 'required|string',
            'event_id' => 'required|integer|exists:events,id',
            'phone' => 'required|string',
            'post' => 'required|string',
            'email' => 'string|email:rfc,dns',
            'organization' => 'required|string',
            'location_country_id' => 'required|integer|exists:location_countries,id',
            'location_city_id' => 'required|integer|exists:location_cities,id',
            'token' => ['required', new ValidRecaptcha]
        ];
    }
}
