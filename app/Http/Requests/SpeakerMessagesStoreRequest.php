<?php

namespace App\Http\Requests;

use App\Rules\ValidRecaptcha;
use Illuminate\Foundation\Http\FormRequest;

class SpeakerMessagesStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->is_sender = 1){
            return [
                'communicator_id' => 'required|integer|exists:speakers,id',
                'text'            => 'required|string',
            ];
        }
        elseif($this->is_sender = 0){
            return [
                'communicator_id' => 'required|integer|exists:speakers,id',
                'text'            => 'required|string',
                'parent_id'       => 'required|integer|exists:speaker_messages,id',
            ];
        }

    }
}
