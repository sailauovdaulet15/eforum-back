<?php

namespace App\Http\Requests;

use App\Rules\ValidRecaptcha;
use Illuminate\Foundation\Http\FormRequest;

class CommentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(auth('api')->user()){
            return [
                'content' => 'required|string|between:1,5000',
                'event_slug' => 'required|exists:events,slug',
                'parent_id' => 'integer|exists:comments,id',
                'email' => 'string|email',
                'full_name' => 'string|between:2,200',
                'token' => ['required', new ValidRecaptcha]
            ];
        }
        return [
            'content' => 'required|string|between:1,5000',
            'event_slug' => 'required|exists:events,slug',
            'parent_id' => 'integer|exists:comments,id',
            'full_name' => 'required|string|between:2,200',
            'email' => 'required|string|email',
            'token' => ['required', new ValidRecaptcha]
        ];
    }
}
