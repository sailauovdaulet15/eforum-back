<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'full_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'location_country_id' => 'integer|exists:location_countries,id',
            'location_city_id' => 'integer|exists:location_cities,id',
            'company' => 'required|string|max:255',
            'post' => 'required|string|max:255',
            'is_notification_events' => 'nullable|boolean'
        ];
    }

    public function messages(): array
    {
        return [
            '*.required' => 'Поле обязательно для заполнения',
            '*.string' => 'Поле должно быть строкой',
        ];
    }
}
