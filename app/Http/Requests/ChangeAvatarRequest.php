<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeAvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'avatar' => 'required|mimes:jpeg,jpg,png',
        ];
    }

    public function messages(): array
    {
        return [
            'mimes' => 'Формат принимаемых файлов: jpeg,jpg,png',
            '*.required' => 'Поле обязательно для заполнения',
        ];
    }
}
