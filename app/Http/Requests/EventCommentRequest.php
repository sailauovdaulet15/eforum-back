<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventCommentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'event_id' => 'required|integer',
            'speaker_id' => 'required|integer',
            'comment_text' => 'required|string',
        ];
    }
}
