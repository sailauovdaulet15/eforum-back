<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComplaintStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(auth('api')->user()){
            return [
                'comment_id' => 'required|integer',
                'handbook_type_complaint_id' => 'required|integer',
            ];
        }
        return [
            'full_name' => 'required|string',
            'email' => 'required|email',
            'comment_id' => 'required|integer',
            'handbook_type_complaint_id' => 'required|integer',
        ];
    }
}
