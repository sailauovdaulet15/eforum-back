<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email:rfc,dns|unique:users',
            'phone' => 'required|string',
            'location_country_id' => 'integer|exists:location_countries,id',
            'location_city_id' => 'integer|exists:location_cities,id',
            'password' => 'required|string|min:8',
            'password_confirm' => 'required|string|min:8|same:password',
            'isSpeaker' => 'nullable|boolean',
            'company' => 'required|string',
            'post' => 'required|string'
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Пользователь с таким email не зарегистрирован',
            '*.required' => 'Поле обязательно для заполнения',
            '*.string' => 'Поле должно быть строкой',
        ];
    }
}
