<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController as BaseVoyagerBreadController;

class VoyagerBreadController extends BaseVoyagerBreadController
{
    public function orderRow($position, $table, $sort, $direction, $id)
    {
        $dataType = Voyager::model('DataType')->where('slug', '=', $table)->first();
        $orderGroup = [];
        if(isset($dataType->details->order_group) && is_array($dataType->details->order_group)){
            foreach ($dataType->details->order_group as $orderGroupItem){
                $orderGroup[] = $orderGroupItem;
            }
        }

        $item = \DB::table($table)->where(['id' => $id])->select(array_merge($orderGroup, ['id']))->first();

        $orderGroup = [];
        if(isset($dataType->details->order_group) && is_array($dataType->details->order_group)) {
            foreach ($dataType->details->order_group as $orderGroupItem) {
                $orderGroup[$orderGroupItem] = $item->$orderGroupItem;
            }
        }
        $checkDeleteAt = Schema::hasColumn($table, 'deleted_at');
        if ($checkDeleteAt){
            $orderGroup = array_merge($orderGroup, ['deleted_at' => null]);
        }
        $count = \DB::table($table)->where($orderGroup)->count();


        if ($count > 1) {
            if (($sort==='DESC' and $direction==="UP" and $position!=$count) or
                ($sort==='ASC' and $direction==="DOWN" and $position!=$count)) {

                \DB::table($table)->where(array_merge($orderGroup, ['position' => $position + 1]))
                    ->update(['position' => $position]);

                \DB::table($table)->where(array_merge($orderGroup, ['id' => $id]))
                    ->update(['position' => $position + 1]);

            } else if (($sort==='DESC' and $direction==="DOWN" and $position!=1) or
                       ($sort==='ASC' and $direction==="UP" and $position!=1)) {

                \DB::table($table)->where(array_merge($orderGroup, ['position' => $position - 1]))
                    ->update(['position' => $position]);

                \DB::table($table)->where(array_merge($orderGroup, ['id' => $id]))
                    ->update(['position' => $position - 1]);

            } else if (($sort==='DESC' and $direction==="FIRST" and $position!=$count) or
                       ($sort==='ASC' and $direction==="LAST" and $position!=$count)) {

                \DB::table($table)->where('position', '>', $position)
                    ->where($orderGroup)
                    ->update(['position' => \DB::raw('position-1')]);

                \DB::table($table)->where(array_merge($orderGroup, ['id' => $id]))
                    ->update(['position' => $count]);

            } else if (($sort==='DESC' and $direction==="LAST" and $position!=1) or
                       ($sort==='ASC' and $direction==="FIRST" and $position!=1)) {

                \DB::table($table)->where('position', '<', $position)
                    ->where($orderGroup)
                    ->update(['position' => \DB::raw('position+1')]);

                \DB::table($table)->where(array_merge($orderGroup, ['id' => $id]))
                    ->update(['position' => 1]);
            }
        }

        return redirect()->route("voyager.$table.index");
    }
}
