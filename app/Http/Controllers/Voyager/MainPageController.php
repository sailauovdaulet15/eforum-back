<?php

namespace App\Http\Controllers\Voyager;


use App\Models\MainPage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class MainPageController extends VoyagerBaseController
{
    public function update(Request $request, $id): RedirectResponse
    {
        if($this->isMore()){
            return redirect()->back()->with([
                'message'    => 'Нельзя создавать больше 1 активных записей',
                'alert-type' => 'error',
            ]);
        }
        return parent::update($request, $id);
    }

    public function store(Request $request): RedirectResponse
    {
        if($this->isMore()){
            return redirect()->back()->with([
                'message'    => 'Нельзя создавать больше 1 активных записей',
                'alert-type' => 'error',
            ]);
        }
        return parent::store($request);
    }

    public function restore(Request $request, $id): RedirectResponse
    {
        if($this->isMore()){
            return redirect()->back()->with([
                'message'    => 'Нельзя создавать больше 1 активных записей',
                'alert-type' => 'error',
            ]);
        }
        return parent::restore($request, $id);
    }

    private function isMore():bool
    {
        return MainPage::all()->count() >= 1;
    }
}
