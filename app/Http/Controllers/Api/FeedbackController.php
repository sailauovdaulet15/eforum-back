<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackRequest;
use App\Http\Resources\FeedbackResource;
use App\Models\Feedback;

class FeedbackController extends Controller
{
    public function store(FeedbackRequest $request)
    {
        $item = Feedback::create($request->all());

        return new FeedbackResource($item);
    }
}
