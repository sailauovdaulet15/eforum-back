<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsSlugResource;
use App\Models\News;
use App\Http\Resources\NewsResource;

class NewsController extends Controller
{
    private const PAGINATION_COUNT = 9;

    public function index()
    {
        $perPage = request()->exists('per_page') ? intval(request()->get('per_page')) : self::PAGINATION_COUNT;
        $items = News::orderBy('created_at', 'desc')->paginate($perPage);

        return NewsResource::collection($items);
    }

    public function slug(string $slug)
    {
        $item = News::where('seo_slug', $slug)->first();
        if (!$item) {
            return response()->json(['data' => ['message' => 'News not found']], 404);
        }

        return new NewsSlugResource($item);
    }
}
