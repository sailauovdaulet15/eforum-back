<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentChildResource;
use App\Http\Resources\CommentResource;
use App\Http\Requests\CommentStoreRequest;
use App\Models\Comment;
use App\Providers\CommentService;



class CommentController extends Controller
{
    private CommentService $commentService;

    public function __construct(CommentService $commentService){
        $this->commentService = $commentService;
    }

    public function index(){
        return CommentResource::collection(Comment::where('is_active', 1)->get());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentStoreRequest $request):CommentResource
    {
        $result = $this->commentService->store($request->all());
        return new CommentResource($result);
    }
    public function getUserComments(){
        return $this->commentService->setArray();
    }
    public function getCommentsByEvent($slug)
    {
        $result = Comment::with('event')
            ->whereHas('event', function($q) use ($slug){
                $q->where('slug', $slug);
            })
            ->where('parent_id', null)
            ->where('is_active', 1)
            ->orderBy('id', 'desc')
            ->with('child')
            ->get();

        return CommentChildResource::collection($result);
    }
}
