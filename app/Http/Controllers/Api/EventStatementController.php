<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventStatementRequest;
use App\Http\Resources\EventStatementResource;
use App\Mail\EventStatements;
use App\Models\EventStatement;
use App\Providers\EventStatementService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;

class EventStatementController extends Controller
{
    protected EventStatementService $eventStatementService;
    public function __construct(EventStatementService $eventStatementService){
        $this->eventStatementService = $eventStatementService;
    }
    public function store(EventStatementRequest $request)
    {
        $item = $this->eventStatementService->store($request->all());
        if ($item) {
            Mail::to($item->email)->send(new EventStatements($item));
        }
        return new EventStatementResource($item);
    }

    public function show(int $id)
    {
        $item = EventStatement::findOrFail($id);

        return new EventStatementResource($item);
    }
}
