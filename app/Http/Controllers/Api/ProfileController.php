<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeAvatarRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\UserResource;
use App\Models\Speaker;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function profileUpdate(UpdateProfileRequest $request)
    {
        $userId = auth()->user()->id;
        $user = User::findOrFail($userId);
        $user->full_name = $request->full_name;
        $user->phone = $request->phone;
        $user->location_country_id = $request->location_country_id;
        $user->location_city_id = $request->location_city_id;
        $user->company = $request->company;
        $user->post = $request->post;

        if ($request->is_notification_events == true) {
            $user->is_notification_events = true;
        } else {
            $user->is_notification_events = false;
        }

        $user->save();

        return new UserResource($user);
    }

    public function changeAvatar(ChangeAvatarRequest $request)
    {
        if ($request->hasFile('avatar')) {
            $storePath = $request->file('avatar')
                ->store('users/' . date('F') . date('Y'));
            $user = auth()->user();
            if ($user->isSpeaker && $user->speaker_id) {
                $speaker = Speaker::findOrFail($user->speaker_id);
                $speaker->update([
                    'photo' => $storePath
                ]);
            } else {
                $user->update([
                    'avatar' => $storePath
                ]);
            }


            return new UserResource($user);
        }
        return response(['error' => 'Загрузите файл',], 400);
    }

    public function deleteAvatar()
    {
        $userId = auth()->user()->id;
        $user = User::findOrFail($userId);
        if (Storage::exists($user->avatar)) {
            Storage::delete($user->avatar);
        }
        $user->update([
            'avatar' => null
        ]);
        return new UserResource($user);
    }
}
