<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Mail\VerifyUser ;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['register', 'login']]);
    }

    public function register(RegisterRequest $request): JsonResponse
    {
        $time = time();
        $baseUrlFront = env('APP_URL_FRONT', 'https://eforum-front.rocketfirm.net');
        $baseUrl = env('APP_URL', 'https://eforum.rocketfirm.net');
        $jwtToken = (new Builder())
            ->issuedBy($baseUrl)
            ->permittedFor($baseUrlFront)
            ->relatedTo($request->email)
            ->issuedAt($time)
            ->canOnlyBeUsedAfter($time + 60)
            ->expiresAt($time + 600)
            ->getToken();
        $jwtToken = (new Parser())->parse((string)$jwtToken);
        $jwtToken->getPayload();
        $this->redisSet($request->all());

        Mail::to($request->email)->send(new VerifyUser($jwtToken, $request->email));
        return response()->json([
            'status' => true,
            'message' => 'На указанную почту отправлено письмо для активаций аккаунта',
        ], 201);
    }

    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email:rfc,dns',
            'password' => 'required|string|min:8',
        ]);
        $user = User::where('email', $request->email)->first();
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (!$user) {
            return response()->json([
                'status' => 'false',
                'message' => 'Пользователь не найден'
            ], 422);
        }

        if (!Hash::check($request->password, $user->password))
        {
            return response()->json([
                'status' => 'false',
                'message' => 'Пароль введен не правильно'
            ], 422);
        }

        if (!$token = auth('api')->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        if (!$user->email_verified_at) {
            return response()->json([
                'status' => 'false',
                'error' => 'warning',
                'message' => 'Вам необходимо активировать свою учетную запись. Мы отправили вам ссылку для активации аккаунта, пожалуйста, проверьте свою электронную почту.'
            ], 422);
        }

        return $this->createNewToken($token);
    }

    public function logout(): JsonResponse
    {
        auth('api')->logout();

        return response()->json(['message' => 'Пользователь успешно вышел из системы']);
    }

    public function refreshToken(): JsonResponse
    {
        return $this->createNewToken(auth()->refresh());
    }

    public function profile(): UserResource
    {
        $user = auth()->user();
        return (new UserResource($user));
    }

    protected function createNewToken(string $token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => new UserResource(auth('api')->user()),
        ]);
    }

    private function redisSet(array $data)
    {
        $email = $data['email'];
        Redis::hmSet($email, $data);
    }
}
