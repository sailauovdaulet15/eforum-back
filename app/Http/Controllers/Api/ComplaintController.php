<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ComplaintStoreRequest;
use App\Http\Resources\ComplaintResource;
use App\Providers\ComplaintService;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    private ComplaintService $complaintService;

    public function __construct(ComplaintService $complaintService){
        $this->complaintService = $complaintService;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComplaintStoreRequest $request):ComplaintResource
    {
      $result = $this->complaintService->store($request->all());
       return new ComplaintResource($result);
    }
}
