<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SpeakerMessagesStoreRequest;
use App\Http\Resources\SpeakerMessagesResource;
use App\Http\Resources\SpeakerMessagesStoreResource;
use App\Services\SpeakerMessagesService;
use App\Models\SpeakerMessage;

class SpeakerMessagesController extends Controller
{
    protected SpeakerMessagesService $messagesService;

    public function __construct(SpeakerMessagesService $messagesService){
        $this->messagesService = $messagesService;
    }

    public function getMessages(){
        $result = $this->messagesService->getMessagesList();
        return SpeakerMessagesResource::collection($result);
    }

    public function getRepliesMessages(int $id){
        $result = $this->messagesService->getRepliesList($id);
        return new SpeakerMessagesResource($result);
    }
    public function store(SpeakerMessagesStoreRequest $request):SpeakerMessagesStoreResource
    {
        $result = $this->messagesService->store($request->all());

        return new SpeakerMessagesStoreResource($result);
    }
}
