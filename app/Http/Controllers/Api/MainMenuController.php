<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MainMenuResource;
use App\Models\MainMenu;
use Illuminate\Http\Request;

class MainMenuController extends Controller
{
    public function index()
    {
        $items = MainMenu::orderBy('position', 'asc')->get();

        return MainMenuResource::collection($items);
    }
}
