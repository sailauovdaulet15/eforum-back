<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use Carbon\Carbon;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Hash;
use Lcobucci\JWT\Parser;

class ResetPasswordController extends Controller
{
    public function reset($token, ResetPasswordRequest $request)
    {
        $jwtToken = (new Parser())->parse((string)$token);
        $token = $jwtToken->getPayload();
        $email = $jwtToken->getClaim('sub');
        $tokenTime = $jwtToken->getClaim('iat');
        $expiredToken = $jwtToken->getClaim('exp');
        $resetEmail = PasswordReset::where('email', $email)->firstOrFail();

        if ($tokenTime >= $expiredToken) {
            $resetEmail->status_id = PasswordReset::STATUS_PASSWORD_NOT_UPDATED;
            $resetEmail->save();
            return response()->json([
                'status' => 'false',
                'message' => 'Ссылка не действительна'
            ]);
        } elseif ($token) {
            $user = User::where('email', $resetEmail->email)->firstOrFail();
            if (Hash::check($request->password, $user->password)) {
                return response()->json([
                    'status' => 'false',
                    'message' => 'Новый пароль и старый пароль совпадают'
                ], 422);
            } else {
                $user->update([
                    'password' => Hash::make($request->password)
                ]);
                $resetEmail->status_id = PasswordReset::STATUS_PASSWORD_SUCCESS_UPDATED;
                $resetEmail->save();
                return response()->json([
                    'status' => 'true',
                    'message' => 'Пароль успешно обновлен'
                ]);
            }
        }
    }
}
