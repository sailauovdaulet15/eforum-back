<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SpeakerResource;
use App\Services\SpeakerService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SpeakerController extends Controller
{
    protected SpeakerService $speakerService;

    public function __construct(SpeakerService $speakerService)
    {
        $this->speakerService = $speakerService;
    }

    public function getSpeakerByEvent(string $slug):AnonymousResourceCollection
    {

        return SpeakerResource::collection($this->speakerService->getSpeakerList($slug));

    }
}
