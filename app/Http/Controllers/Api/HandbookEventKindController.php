<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\HandbookEventKindResource;
use App\Models\HandbookEventKind;

class HandbookEventKindController extends Controller
{
    public function open()
    {
        $item = HandbookEventKind::firstWhere('id', 1);

        return new HandbookEventKindResource($item);
    }

    public function close()
    {
        $item = HandbookEventKind::firstWhere('id', 2);

        return new HandbookEventKindResource($item);
    }
}
