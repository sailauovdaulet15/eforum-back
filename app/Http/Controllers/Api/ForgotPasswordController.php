<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;


class ForgotPasswordController extends Controller
{
    public function forgot(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email:rfc,dns',
        ]);
        $time = time();
        $baseUrlFront = env('APP_URL_FRONT', 'https://eforum-front.rocketfirm.net');
        $baseUrl = env('APP_URL', 'https://eforum.rocketfirm.net');
        $jwtToken = (new Builder())
            ->issuedBy($baseUrl) // Configures the issuer (iss claim)
            ->permittedFor($baseUrlFront) // Configures the audience (aud claim)
            ->relatedTo($request->email)
            ->issuedAt($time) // Configures the time that the token was issue (iat claim)
            ->canOnlyBeUsedAfter($time + 60) // Configures the time that the token can be used (nbf claim)
            ->expiresAt($time + 600) // Configures the expiration time of the token (exp claim)
            ->getToken(); // Retrieves the generated token
        $jwtToken = (new Parser())->parse((string)$jwtToken); // Parses from a string
        $jwtToken->getPayload();
        $email = User::where('email', $request->email)->first();
        if(!$email)
        {
            return response()->json([
                'status' => false,
                'message' => 'Пользователь не найден'
            ]);
        }else {
            $user = PasswordReset::create([
                'email' => $request->email,
                'status_id' => PasswordReset::STATUS_SUCCESS_SENT
            ]);

            Mail::to($user->email)->send(new ResetPassword($jwtToken, $user->email));

            return response()->json([
                'status' => true,
                'message' => 'Успешно отправлен, проверьте почту'
            ]);
        }
    }

}
