<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\LocationCountryResource;
use App\Models\LocationCountry;

class LocationCountryController extends Controller
{
    public function index()
    {
        $countries = LocationCountry::all();

        return LocationCountryResource::collection($countries);
    }
}
