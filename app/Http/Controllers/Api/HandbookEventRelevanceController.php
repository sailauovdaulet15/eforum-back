<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\RelevanceResource;
use App\Models\HandbookEventRelevance;

class HandbookEventRelevanceController
{
    public function index()
    {
        $items = HandbookEventRelevance::all();

        return RelevanceResource::collection($items);
    }
}
