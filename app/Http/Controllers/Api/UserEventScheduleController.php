<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\EventSchedule;
use App\Services\UserEventScheduleService;


class UserEventScheduleController extends Controller
{
    private const SPEAKER = 1;

    protected UserEventScheduleService $userEventSchedule;
    protected EventSchedule $eventSchedule;

    public function __construct(UserEventScheduleService $userEventSchedule,
                                EventSchedule $eventSchedule)
    {
        $this->userEventSchedule = $userEventSchedule;
        $this->eventSchedule = $eventSchedule;
    }

    public function schedule(): array
    {
        if (request()->get('role') == self::SPEAKER) {
            return $this->eventSchedule->getSpeakerEventSchedule();
        }
        return $this->userEventSchedule->getUserEventSchedule();

    }
}
