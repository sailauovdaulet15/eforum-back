<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\LocationCityResource;
use App\Models\LocationCity;

class LocationCityController extends Controller
{
    public function index()
    {
        $cities = LocationCity::all();

        return LocationCityResource::collection($cities);
    }
}
