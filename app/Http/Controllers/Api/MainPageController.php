<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MainPageResource;
use App\Models\MainPage;

class MainPageController extends Controller
{
    public function index()
    {
        $items = MainPage::all();

        return MainPageResource::collection($items);
    }
}
