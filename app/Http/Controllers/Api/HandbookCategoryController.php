<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\HandbookCategory;

class HandbookCategoryController extends Controller
{
    public function index()
    {
        $items = HandbookCategory::all();

        return CategoryResource::collection($items);
    }
}
