<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\HandbookTypeComplaintResource;
use App\Models\HandbookTypeComplaint;
use Illuminate\Http\Request;

class HandbookComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $result = HandbookTypeComplaint::orderBy('position', 'desc')->get();
        return HandbookTypeComplaintResource::collection($result);
    }
}
