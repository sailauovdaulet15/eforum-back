<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\LocationCity;
use App\Models\Speaker;
use App\Models\User;
use App\Models\VerifyUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Lcobucci\JWT\Parser;

class VerifyUserController extends Controller
{
    public function verifyUser($token)
    {
        $jwtToken = (new Parser())->parse((string)$token);
        $email = $jwtToken->getClaim('sub');
        $tokenTime = $jwtToken->getClaim('iat');
        $expiredToken = $jwtToken->getClaim('exp');
        $emailSet = $this->redisGet($email);
        if ($tokenTime != $expiredToken && $emailSet) {
            $now = Carbon::now();

            $user = User::firstOrNew([
                'full_name' => $emailSet['name'],
                'email' => $emailSet['email'],
                'email_verified_at' => $now->format('Y-m-d H:i:s'),
                'phone' => $emailSet['phone'],
                'password' => Hash::make($emailSet['password']),
                'company' => $emailSet['company'],
                'post' => $emailSet['post'],
                'location_country_id' => $emailSet['location_country_id'] ?? null,
                'location_city_id' => $emailSet['location_city_id'] ?? null,
                'date_time_last_login' => $now,
                'account_is_active' => true,
            ]);

            if ($emailSet['isSpeaker'] == true) {
                $speaker = Speaker::create([
                    'full_name' => $user->full_name,
                ]);
                $user->speaker_id = $speaker->id;
                $user->isSpeaker = false;
            }

            $user->save();
        }else {
            return response()->json([
                'status' => 'false',
                'message' => 'Ссылка недействительна'
            ]);
        }

        Redis::del($email);

        return response()->json([
            'status' => 'true',
            'message' => 'Ваша почта подтверждена. Можете авторизоваться.'
        ]);
    }

    private function redisGet($email): ?array
    {
        if(!Redis::hGet($email, 'email')){
            return null;
        }
       return Redis::hgetAll($email);
    }
}
