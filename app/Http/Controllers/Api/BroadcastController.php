<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BroadcastUserResource;
use App\Providers\BroadcastService;

class BroadcastController extends Controller
{
    protected BroadcastService $broadcastService;

    public function __construct(BroadcastService $broadcastService){
        $this->broadcastService = $broadcastService;
    }

    public function userList(){
      return BroadcastUserResource::collection($this->broadcastService->getUserBroadcast());
    }
}
