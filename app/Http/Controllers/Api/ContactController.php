<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Contact;


class ContactController extends Controller
{
    public function index()
    {
        $item = Contact::first();
        if(!$item){
            return response()->json(['data' => ['message' => 'Контакты не найдены']], 404);
        }
        return [
            "text" => $item->text1,
            "mail" => [
                "title" => $item->text2,
                "email" => $item->email
            ],
            "social" => [
                "title" => $item->text3,
                "icons" => $item->getIcons()
            ]
        ];
    }
}
