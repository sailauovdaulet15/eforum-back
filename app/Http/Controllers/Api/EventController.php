<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EventEasyResource;
use App\Http\Resources\EventGoogleScheduleResource;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Services\EventList;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;


class EventController extends Controller
{
    protected EventList $eventList;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct(EventList $eventList)
    {
        $this->eventList = $eventList;
    }

    public function index()
    {
        return $this->eventList->list();
    }

    public function view(string $slug)
    {
        $item = Event::where('slug', $slug)->first();
        if (!$item) {
            return response()->json(['data' => ['message' => 'Event not found']], 404);
        }
        $item->dateStart();//тут проверяем если мероприятие прошло уже то меняем статус на завершенный

        return new EventResource($item);
    }

    public function eventSchedule(): AnonymousResourceCollection
    {
        $data = $this->eventList->getEventSchedule();
        return EventGoogleScheduleResource::collection($data);
    }

    public function getEasyEvent()
    {
        return EventEasyResource::collection($this->eventList->takeEasy());
    }

    public function programs(string $slug)
    {
        return $this->eventList->programs($slug);
    }

    public function speakers(string $slug)
    {
        return $this->eventList->speakers($slug);
    }

    public function organizers(string $slug)
    {
        return $this->eventList->organizers($slug);
    }
}
