<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EventTypeResource;
use App\Models\HandbookEventType;

class HandbookEventTypeController extends Controller
{
    public function index()
    {
        $items = HandbookEventType::all();

        return EventTypeResource::collection($items);
    }
}
