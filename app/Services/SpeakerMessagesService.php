<?php


namespace App\Services;


use App\Mail\SpeakerMessagesMailable;
use App\Models\Speaker;
use App\Models\SpeakerMessage;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class SpeakerMessagesService
{
    public function store(array $data)
    {
        $result = SpeakerMessage::query()->create(array_merge($data, $this->setRequestWithToken($data)));
        return $result;
    }

    public function getMessagesList()
    {
        $user = auth('api')->user();
        $speaker = Speaker::findOrFail($user->speaker_id);
        $messagesList = SpeakerMessage::where('parent_id', '=', null)
            ->where('is_active', 1)
            ->where(function ($q) use ($speaker) {
                $q->where('sender_id', '=', $speaker->id)
                    ->orWhere('recipient_id', '=', $speaker->id);
            })->orderBy('id', 'desc')
            ->paginate(6);
        return $messagesList;
    }

    public function getRepliesList($id)
    {
        $messages = SpeakerMessage::findOrFail($id);
        $messages->type = 1;
        return $messages;
    }

    public function setRequestWithToken(array $data): array
    {
        if ($user = auth('api')->user()) {
            $speaker = Speaker::findOrFail($user->speaker_id);
            if ($data['is_sender'] == 1) {
                return ['sender_id' => $speaker->id, 'recipient_id' => $data['communicator_id']];
            }
            return ['sender_id' => $speaker->id, 'recipient_id' => $data['communicator_id']];
        }
        return [];
    }
}
