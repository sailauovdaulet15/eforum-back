<?php


namespace App\Services;


use App\Models\Event;
use App\Models\Speaker;
use App\Models\SpeakerEvent;
use Illuminate\Database\Eloquent\Collection;

class SpeakerService
{
    public function getSpeakerList(string $slug):Collection
    {
        $event = Event::where('slug', '=', $slug)->first();
        $speakerEvent = SpeakerEvent::where('event_id', '=', $event->id)->get()->pluck('speaker_id');
        return Speaker::whereIn('id', $speakerEvent)->get();
    }
}
