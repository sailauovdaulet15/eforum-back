<?php

namespace App\Services;

use App\Http\Resources\EventsResource;
use App\Http\Resources\OrganizerEventsResource;
use App\Http\Resources\ProgramsResource;
use App\Http\Resources\SpeakerEventResource;
use App\Models\Event;
use App\Models\EventProgram;
use App\Models\EventStatement;
use App\Models\HandbookCategory;
use App\Models\OrganizerEvent;
use App\Models\SpeakerEvent;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class EventList
{
    private const PAGINATION_COUNT = 7;

    public function list()
    {
        $category = request()->get('categories');
        $eventType = request()->get('eventType');
        $relevance = request()->get('relevance');
        $open = request()->get('open');
        $close = request()->get('close');
        $perPage = request()->exists('per_page') ? intval(request()->get('per_page')) : self::PAGINATION_COUNT;

        $items = Event::with([
            'handbookCategory',
            'handbookEventType',
            'handbookEventRelevance',
            'handbookEventKind'
        ])->where('is_active', 1)
            ->where(function ($query) use ($category, $eventType, $relevance, $open, $close) {
                if (!empty($category)) {
                    $query->where('handbook_category_id', $category);
                }
                if (!empty($eventType)) {
                    $query->where('handbook_event_type_id', $eventType);
                }
                if (!empty($relevance)) {
                    $query->where('handbook_event_relevance_id', $relevance);
                }
                if (!empty($open || $close)) {
                    if ($open && $close) {
                        $query->where('handbook_event_kind_id', 1)
                            ->orWhere('handbook_event_kind_id', 2);
                    } elseif ($open && !$close) {
                        $query->where('handbook_event_kind_id', 1);
                    } elseif (!$open && $close) {
                        $query->where('handbook_event_kind_id', 2);
                    }
                }

            })
            ->orderBy('position', 'desc')
            ->paginate($perPage);

        return EventsResource::collection($items);
    }

    public function programs(string $slug)
    {
        $event = Event::where('slug', $slug)->first();
        if (!$event) {
            return response()->json(['data' => ['message' => 'Event not found']], 404);
        }

        $eventPrograms = EventProgram::where('event_id', $event->id)->get();
        if (!$eventPrograms) {
            return response()->json(['data' => ['message' => 'EventPrograms not found']], 404);
        }

        $array = [];
        foreach ($eventPrograms as $eventProgram) {
            $array[$eventProgram->date][$eventProgram->handbook_hall_id][] = $eventProgram;
        }
        ksort($array);
        $result = [];
        foreach ($array as $date => $halls) {
            $temp = [];
            foreach ($halls as $hallId => $items) {
                $temp[] = [
                    'hall' => [
                        'id' => $hallId,
                        'name' => $items[0]->handbookHall->name ?? null,
                    ],
                    'items' => ProgramsResource::collection($items)
                ];
            }
            $result[] = [
                'date' => Carbon::create($date)->timestamp,
                'items' => $temp
            ];
        }
        return $result;
    }

    public function speakers(string $slug)
    {
        $event = Event::where('slug', $slug)->first();

        if (!$event) {
            return response()->json(['data' => ['message' => 'Event not found']], 404);
        }

        $speakers = SpeakerEvent::where('event_id', $event->id)->get();

        if (!$speakers) {
            return response()->json(['data' => ['message' => 'SpeakersEvent not found']], 404);
        }

        return SpeakerEventResource::collection($speakers);
    }

    public function organizers(string $slug)
    {
        $event = Event::where('slug', $slug)->first();

        if (!$event) {
            return response()->json(['data' => ['message' => 'Event not found']], 404);
        }

        $organizers = OrganizerEvent::where('event_id', $event->id)->orderBy('position', 'desc')->get();

        if (!$organizers) {
            return response()->json(['data' => ['message' => 'Organizers not found']], 404);
        }

        return OrganizerEventsResource::collection($organizers);
    }


    public function getEventSchedule(): array
    {

        $user = auth('api')->user();
        $eventStatement = EventStatement::where('user_id', '=', $user->id)->get()->pluck('event_id');
        $events = Event::whereIn('id', $eventStatement)
            ->select('id', 'title', 'handbook_category_id', 'short_description', 'date_start', 'date_end')
            ->get();
        $result = [];
        foreach ($events as $event) {
            $startDate = Carbon::createFromFormat('Y-m-d', $event->date_start);
            $endDate = Carbon::createFromFormat('Y-m-d', $event->date_end);
            $period = CarbonPeriod::create($startDate, $endDate);
            $dates = $period->toArray();
            $eventArray = $event->toArray();
            $eventArray['handbook_category_id'] = $event->handbookCategory;
            foreach ($dates as $date) {
                $eventPrograms = EventProgram::where('event_id', '=', $event->id)
                    ->where('date', '=', $date->toDateString())
                    ->select('time_from');
                $items = array_merge($eventArray, [
                    'current_date' => $date->toDateString(),
                    'min_time_from' => $eventPrograms->min('time_from'),
                    'min_time_to' => $eventPrograms->max('time_to')
                ]);
                array_push($result, $items);
            }
        }
        return $result;
    }

    public function takeEasy()
    {
        return Event::select('id', 'slug', 'title')->get();
    }
}
