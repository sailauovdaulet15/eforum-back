<?php


namespace App\Services;


use App\Models\Event;
use App\Models\EventStatement;
use App\Models\Speaker;
use App\Models\SpeakerEvent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class EventSchedule
{

    public function getSpeakerEventSchedule(): array
    {
        $user = auth('api')->user();
        $speaker = Speaker::findOrFail($user->speaker_id);
        $category = request()->get('categories');
        $eventType = request()->get('eventType');
        $relevance = request()->get('relevance');
        $year = request()->get('year');
        $month = request()->get('month');
        $speakerEvent = SpeakerEvent::with('event')
            ->where('speaker_id', '=', $speaker->id)
            ->get()
            ->pluck('event_id');

        $events = Event::select('events.*')
            ->whereIn('events.id', $speakerEvent)
            ->leftJoin('event_programs', 'event_programs.event_id', '=', 'events.id')
            ->where(function ($query) use ($category, $eventType, $relevance) {
                if (!empty($category)) {
                    $query->where('handbook_category_id', $category);
                }
                if (!empty($eventType)) {
                    $query->where('handbook_event_type_id', $eventType);
                }
                if (!empty($relevance)) {
                    $query->where('handbook_event_relevance_id', $relevance);
                }
            })
            ->orderBy('event_programs.time_from')
            ->get()
            ->unique('id');

        $result = [];
        foreach ($events as $item) {
            $this->merge($result, $item->getAllDates($year, $month, $item->id));
        }
        return $result;
    }

    private function merge(&$result, $array)
    {
        if (empty($result)) {
            $result = $array;
            return;
        }

        foreach ($array as $keyYear => $itemYear) {
            foreach ($itemYear as $keyMonth => $itemMonth) {
                foreach ($itemMonth as $keyDay => $itemDay) {
                    foreach ($itemDay as $item) {
                        $result[$keyYear][$keyMonth][$keyDay][] = $item;
                    }
                }
            }
        }
    }




}
