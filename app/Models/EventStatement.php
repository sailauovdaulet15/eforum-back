<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * App\Models\EventStatement
 *
 * @property int $id
 * @property string surname Фамилия
 * @property int $event_id Мероприятия
 * @property string $phone Номер телефон
 * @property string $email Адрес эл. почты
 * @property string $post Должность
 * @property string $organization Организация
 * @property string $location_country_id Id страны
 * @property string $location_city_id Id города
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Event $event
 * @property-read User $user
 * @method static Builder|EventStatement newModelQuery()
 * @method static Builder|EventStatement newQuery()
 * @method static Builder|EventStatement onlyTrashed()
 * @method static Builder|EventStatement query()
 * @method static Builder|EventStatement whereCreatedAt($value)
 * @method static Builder|EventStatement whereDeletedAt($value)
 * @method static Builder|EventStatement whereEmail($value)
 * @method static Builder|EventStatement whereEventId($value)
 * @method static Builder|EventStatement whereId($value)
 * @method static Builder|EventStatement wherePhone($value)
 * @method static Builder|EventStatement wherePosition($value)
 * @method static Builder|EventStatement whereUpdatedAt($value)
 * @method static Builder|EventStatement whereUserName($value)
 * @method static Builder|EventStatement withTrashed()
 * @method static Builder|EventStatement withoutTrashed()
 * @mixin \Eloquent
 */
class EventStatement extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'surname',
        'event_id',
        'phone',
        'email',
        'post',
        'organization',
        'location_country_id',
        'location_city_id',
        'recaptcha_token',
        'user_id'
    ];

    protected $appends = [
        'date_start',
        'date_end',
        'handbookCategory',
        'handbookEventType',
        'handbookEventStatus',
        'handbookEventKind',
        'eventProgram',
        'userLink'
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDateStartAttribute()
    {
        $event = $this->event;
        return $event->date_start;
    }

    public function getDateEndAttribute()
    {
        $event = $this->event;
        return $event->date_end;
    }

    public function getHandbookCategoryAttribute()
    {
        $event = $this->event;
        return $event->handbookCategory->name;
    }

    public function getHandbookEventTypeAttribute()
    {
        $event = $this->event;
        return $event->handbookEventType->name;
    }

    public function getHandbookEventStatusAttribute()
    {
        $event = $this->event;
        return $event->handbookEventStatus->name;
    }

    public function getHandbookEventKindAttribute()
    {
        $event = $this->event;
        return $event->handbookEventKind->name;
    }

    public function getEventProgramAttribute()
    {
        $eventProgram = $this->event->eventPrograms()->orderBy('position', 'desc')->first();
        if($eventProgram)
        {
            return $eventProgram->title;
        }
        return null;
    }

    public function getUserLinkAttribute()
    {
        $user = $this->user;
        return $user->full_name ?? null;
    }
    public function getEventType()
    {
        $event = $this->event;
        return Str::lower($event->handbookEventType->name);
    }
}
