<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

class EventImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'path',
        'event_id'
    ];

    public $timestamps = false;

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public static function deleteFromStorage(int $id): void
    {
        $image = self::find($id);
        Storage::delete($image->path);
        self::destroy($id);
    }
}
