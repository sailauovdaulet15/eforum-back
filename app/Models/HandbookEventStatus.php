<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookEventStatus
 *
 * @property int $id
 * @property string $name
 * @method static Builder|HandbookEventStatus newModelQuery()
 * @method static Builder|HandbookEventStatus newQuery()
 * @method static Builder|HandbookEventStatus query()
 * @method static Builder|HandbookEventStatus whereId($value)
 * @method static Builder|HandbookEventStatus whereName($value)
 * @mixin \Eloquent
 * @property-read Collection|Event[] $events
 * @property-read int|null $events_count
 */
class HandbookEventStatus extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $translatable = [
        'name',
    ];


    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
