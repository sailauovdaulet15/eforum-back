<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Spatial;
use TCG\Voyager\Traits\Translatable;

class HandbookSite extends Model
{
    use HasFactory, SoftDeletes, Translatable, Spatial;

    protected $translatable = [
        'name',
    ];

    protected $spatial = [
        'map',
    ];

    public function handbookHalls(): HasMany
    {
        return $this->hasMany(HandbookHall::class);
    }
}
