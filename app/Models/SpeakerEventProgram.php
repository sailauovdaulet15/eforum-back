<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;

class SpeakerEventProgram extends Model
{
    use HasFactory, SoftDeletes, PositionTrait;

    protected $fillable = [
        'speaker_id',
        'event_id',
        'event_program_id',
        'is_moderator',
        'position'
    ];

    public function eventProgram(): BelongsTo
    {
        return $this->belongsTo(EventProgram::class);
    }

    public function speaker(): BelongsTo
    {
        return $this->belongsTo(Speaker::class);
    }

    public static function boot(): void
    {

        parent::boot();
        self::creating(function(self $speakerEventProgram): void {
            $speakerEventProgram->position = self::where('event_id', $speakerEventProgram->event_id)->count() + 1;
        });

        self::updating(function(self $speakerEventProgram):void {
            if(intval($speakerEventProgram->getOriginal('event_id'))!==intval($speakerEventProgram->event_id)){

                self::where('position', '>', intval($speakerEventProgram->getOriginal('position')))
                    ->where(['event_id' => intval($speakerEventProgram->getOriginal('event_id'))])
                    ->update(['position' => \DB::raw('position-1')]);

                $speakerEventProgram->position = self::where(['event_id' => intval($speakerEventProgram->event_id)])->count()+1;
            }
        });

    }
}
