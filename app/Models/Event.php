<?php

namespace App\Models;


use App\Http\Resources\BroadCastsResource;
use App\Constants\LanguageConstant;
use App\Http\Resources\MaterialsResource;
use App\Http\Resources\ProgramsResource;
use App\Http\Resources\UserEventScheduleDatesResource;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Traits\Translatable;


class Event extends Model
{
    use HasFactory, Translatable, PositionTrait;


    protected $fillable = [
        'title',
        'handbook_category_id',
        'handbook_event_kind_id',
        'handbook_event_status_id',
        'short_description',
        'description',
        'main_image',
        'date_start',
        'date_end',
        'is_active',
        'handbook_event_type_id',
        'payment',
        'location_country_id',
        'location_city_id',
        'handbook_site_id',
        'program',
        'main_image_is_slider',
        'handbook_event_relevance_id',
        'slug',
        'position'
    ];

    protected $translatable = [
        'title',
        'slug'
    ];

    public function handbookCategory(): BelongsTo
    {
        return $this->belongsTo(HandbookCategory::class, 'handbook_category_id');
    }

    public function handbookEventKind(): BelongsTo
    {
        return $this->belongsTo(HandbookEventKind::class);
    }

    public function handbookEventStatus(): BelongsTo
    {
        return $this->belongsTo(HandbookEventStatus::class);
    }

    public function handbookEventType(): BelongsTo
    {
        return $this->belongsTo(HandbookEventType::class);
    }

    public function images(): HasMany
    {
        return $this->hasMany(EventImage::class, 'event_id');
    }

    public function videoRecords(): HasMany
    {
        return $this->hasMany(VideoRecord::class);
    }

    public function handbookEventRelevance(): BelongsTo
    {
        return $this->belongsTo(HandbookEventRelevance::class);
    }

    public function handbookSite(): BelongsTo
    {
        return $this->belongsTo(HandbookSite::class);
    }

    public function locationCountry(): BelongsTo
    {
        return $this->belongsTo(LocationCountry::class);
    }

    public function locationCity(): BelongsTo
    {
        return $this->belongsTo(LocationCity::class);
    }

    public function organizerEvents(): HasMany
    {
        return $this->hasMany(OrganizerEvent::class);
    }

    public function speakerEvents(): HasMany
    {
        return $this->hasMany(SpeakerEvent::class)->orderBy('position', 'desc');
    }

    public function eventPrograms(): HasMany
    {
        return $this->hasMany(EventProgram::class);
    }

    public function broadCasts(): HasMany
    {
        return $this->hasMany(Broadcast::class);
    }

    public function materials(): HasMany
    {
        return $this->hasMany(Material::class);
    }

    public function getBroadCasts(): ?AnonymousResourceCollection
    {
        $current = $this->handbookEventRelevance()->whereIn('id', [1,3])->first();
        $broadcastData = Broadcast::where('event_id', $this->id)->get();
        $broadCasts = collect();
        if ($current) {
           $broadCasts = $broadcastData;
        }
        return BroadCastsResource::collection($broadCasts);
    }

    public function getMaterials(): ?AnonymousResourceCollection
    {
        $relevances = $this->handbookEventRelevance()->where('id', 1)
            ->orWhere('id', 3)->first();
        $materialData = Material::where('event_id', $this->id)->get();
        $materials = collect();
        if ($relevances) {
           $materials = $materialData;
        }
        return MaterialsResource::collection($materials);
    }

    public function getReportCounter(): array
    {
        $planned = $this->handbookEventRelevance()->firstWhere('id', 2);
        $event = Event::with('handbookEventRelevance')->findOrFail($this->id);
        $events = [];
        if($planned)
        {
            $events[] = $event;
        }
        return $events;
    }

    public function getTranslate(string $field, Request $request)
    {
        return $this->getTranslatedAttribute($field, $request->header('Accept-Language'), LanguageConstant::RU);
    }

    public function dateStart(): ?string
    {
        $eventProgram = $this->eventPrograms()->orderBy('position', 'ASC')->first();
        if (!$eventProgram) {
            return null;
        }

        $time = !empty($eventProgram->time_from) ? $eventProgram->time_from : '00:00:00';

        $now = Carbon::now();
        $dateTime = Carbon::createFromFormat('Y-m-d H:i:s', $this->date_start . ' ' . $time);
        $startEvents = 'До начала мероприятия осталось ';
        if ($dateTime > $now) {
            if($now->diff($dateTime)->days > 0){
                return $startEvents. $now->diff($dateTime)->format('%d день(дней) %h ч. %i мин. %s сек.');
            }
            if($now->diff($dateTime)->h > 0){
                return $startEvents. $now->diff($dateTime)->format('%h ч. %i мин. %s сек.');
            }
            if($now->diff($dateTime)->i > 0){
                return $startEvents. $now->diff($dateTime)->format('%i мин. %s сек.');
            }
            return $startEvents. $now->diff($dateTime)->format('%s сек.');
        }

        //если сюда дошли значит мероприятие началось, но может еще идти
        $this->checkRelevance();
        return null;
    }

    function checkRelevance()
    {
        //проверяем закончился последний тайм или нет
        $eventProgram = $this->eventPrograms()->orderBy('position', 'desc')->first();
        if (!$eventProgram) {
            return;
        }

        $time = !empty($eventProgram->time_to) ? $eventProgram->time_to : '00:00:00';
        $now = Carbon::now();
        $dateTime = Carbon::createFromFormat('Y-m-d H:i:s', $this->date_end . ' ' . $time);

        if ($dateTime < $now) {
            $this->handbook_event_relevance_id = 3;
            $this->save();
        }
    }

    public function getImage(): ?string
    {
       return Storage::disk('public')->url('/stubs/_thumb_20678.png');
    }

    public static function boot(): void
    {
        parent::boot();

        self::creating(function(self $event): void {
            $event->position = self::max('position') + 1;
            $event->slug = self::getUniqueSlug($event->slug);
        });

        self::deleting(function(self $event): void {
            $events = self::where('position', '>', $event->position)->get();
            foreach ($events as $item) {
                $item->position--;
                $item->save();
            }
        });
    }

    public static function getEventsHalls($jsonEncode = true)
    {
        $items = Event::with('handbookSite')
            ->with('handbookSite.handbookHalls')
            ->get()
            ->toArray();

        $result = [];
        array_push($result, [
            'id' => 0,
            'name' => 'не выбрано',
            'children' => [],
        ]);
        //мероприятия
        foreach ($items as $item) {
            $temp = [];
            array_push($temp, [
                'id' => 0,
            ]);


            if(!isset($item['handbook_site']['handbook_halls'])){
                continue;
            }
            //залы
            foreach ($item['handbook_site']['handbook_halls'] as $handbookHall) {
                array_push($temp, [
                    'id' => $handbookHall['id'],
                    'name' => $handbookHall['name'],
                ]);
            }

            array_push($result, [
                'id' => $item['id'],
                'name' => $item['title'],
                'children' => $temp,
            ]);
        }

        return $jsonEncode ? json_encode($result) : $result;
    }

    public static function getEventProgram($jsonEncode = true){
        $items = Event::with('eventPrograms')
            ->get()
            ->toArray();

        $result = [];
        array_push($result, [
            'id' => 0,
            'name' => 'не выбрано',
            'children' => [],
        ]);
        //мероприятия
        foreach ($items as $item) {
            $temp = [];
            array_push($temp, [
                'id' => 0,
                'name' => 'не выбрано',
            ]);

            if(!isset($item['event_programs'])){
                continue;
            }
            //залы
            foreach ($item['event_programs'] as $eventPrograms) {
                array_push($temp, [
                    'id' => $eventPrograms['id'],
                    'name' => $eventPrograms['title'],
                ]);
            }

            array_push($result, [
                'id' => $item['id'],
                'name' => $item['title'],
                'children' => $temp,
            ]);
        }

        return $jsonEncode ? json_encode($result) : $result;
    }

    public function getAllDates($yearR = null, $mothR = null) : array
    {
        $dateData = $this->eventPrograms()->orderBy('date', 'desc')->get()->unique('date');
        $result = [];

        foreach($dateData as $eventProgramDates)
        {
            $carbon = Carbon::create($eventProgramDates->date);
            $year = $carbon->format('Y');
            $month = $carbon->format('m');
            $day = $carbon->format('d');

            if($yearR && $year != $yearR) {
                continue;
            }
            if($yearR && $mothR && $month != $mothR) {
                continue;
            }

            $result[$year][$month][$day][] = new UserEventScheduleDatesResource($eventProgramDates->date, $this);
        }

        return $result;
    }

    public function getTimeFrom($date)
    {
        return $this->eventPrograms()->where('date', $date)->select('time_from')->min('time_from');
    }
    public function getTimeTo($date)
    {
        return $this->eventPrograms()->where('date', $date)->select('time_to')->max('time_to');
    }

    public function getUniqueSlug($title): string
    {
        $slug = Str::slug($title, "-");

        if(!self::query()->where('slug', $slug)->exists()){
            return $slug;
        }

        $t = 1;
        while (self::query()->where('slug', $slug.'-'.strval($t))->count() > 0){
            $t++;
        }
        return $slug.'-'.strval($t);
    }
}
