<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;

class OrganizerEvent extends Model
{
    use HasFactory, PositionTrait;

    protected $fillable = [
        'event_id',
        'organizer_id',
    ];

    protected $appends = [
        'posit'
    ];

    public function organizer(): BelongsTo
    {
        return $this->belongsTo(Organizer::class);
    }

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public static function boot(): void
    {
        parent::boot();

        self::creating(function(self $organizerEvent): void {
            $organizerEvent->position = self::where('event_id', $organizerEvent->event_id)->count() + 1;
        });

        self::updating(function(self $organizerEvent):void {
            if(intval($organizerEvent->getOriginal('event_id'))!==intval($organizerEvent->event_id)){

                self::where('position', '>', intval($organizerEvent->getOriginal('position')))
                    ->where(['event_id'=>intval($organizerEvent->getOriginal('event_id')),'deleted_at' => null])
                    ->update(['position' => \DB::raw('position-1')]);

                $organizerEvent->position = self::where(['event_id'=>intval($organizerEvent->event_id),'deleted_at' => null])->count()+1;
            }
        });
    }

    public function handbookTypeSponsor(): BelongsTo
    {
        return $this->belongsTo(HandbookTypeSponsor::class);
    }

    public function getPositAttribute(): ?int
    {
        return $this->position;
    }
}
