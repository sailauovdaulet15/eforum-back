<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Traits\Translatable;

class News extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;

    protected $translatable = [
        'title',
        'content',
        'seo_title',
        'seo_description',
        'seo_slug',
    ];

    protected $fillable = [
        'position',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getImage(): ?string
    {
        if ($this->image_uri == null) {
            return Storage::disk('public')->url('/stubs/_thumb_20678.png');
        } else {
            return Storage::disk(config('voyager.storage.disk'))->url($this->image_uri);
        }
    }
}
