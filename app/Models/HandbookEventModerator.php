<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * App\Models\HandbookEventModerator
 *
 * @property int $id
 * @property string $full_name ФИО модератора
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @method static Builder|HandbookEventModerator newModelQuery()
 * @method static Builder|HandbookEventModerator newQuery()
 * @method static Builder|HandbookEventModerator onlyTrashed()
 * @method static Builder|HandbookEventModerator query()
 * @method static Builder|HandbookEventModerator whereCreatedAt($value)
 * @method static Builder|HandbookEventModerator whereDeletedAt($value)
 * @method static Builder|HandbookEventModerator whereFullName($value)
 * @method static Builder|HandbookEventModerator whereId($value)
 * @method static Builder|HandbookEventModerator whereUpdatedAt($value)
 * @method static Builder|HandbookEventModerator withTrashed()
 * @method static Builder|HandbookEventModerator withoutTrashed()
 * @mixin \Eloquent
 */
class HandbookEventModerator extends Model
{
    use HasFactory, SoftDeletes;
}
