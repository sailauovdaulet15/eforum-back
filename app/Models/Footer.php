<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;

class Footer extends Model
{
    use HasFactory, SoftDeletes, PositionTrait;

    protected $fillable = [
        'position'
    ];

    public static function boot(): void
    {
        parent::boot();

        self::creating(function(self $mainBanner): void {
            $mainBanner->position = self::max('position') + 1;
        });

        self::deleting(function(self $mainBanner): void {
            $mainBanners = self::where('position', '>', $mainBanner->position)->get();
            foreach ($mainBanners as $banner) {
                $banner->position--;
                $banner->save();
            }
        });
    }

    public function getUrl()
    {
        if(empty($this->link)){
            return '';
        }
        if(strpos($this->link, 'https://') !==false || strpos($this->link, 'http://') !==false){
            return $this->link;
        }
        return env('APP_URL_FRONT', 'https://eforum-front.rocketfirm.net'). $this->link;
    }
}
