<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\MainPage
 *
 * @property int $id
 * @property string $title Заголовок
 * @property string $subtitle Подзаголовок
 * @property string|null $video Видео
 * @property string|null $image Изображение
 * @property bool $isImage Если true то изображение, если false то видео
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|MainPage newModelQuery()
 * @method static Builder|MainPage newQuery()
 * @method static Builder|MainPage query()
 * @method static Builder|MainPage whereCreatedAt($value)
 * @method static Builder|MainPage whereId($value)
 * @method static Builder|MainPage whereImage($value)
 * @method static Builder|MainPage whereIsImage($value)
 * @method static Builder|MainPage whereSubtitle($value)
 * @method static Builder|MainPage whereTitle($value)
 * @method static Builder|MainPage whereUpdatedAt($value)
 * @method static Builder|MainPage whereVideo($value)
 * @method static Builder|MainPage whereDeletedAt($value)
 * @mixin \Eloquent
 */
class MainPage extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'subtitle',
        'video',
        'image',
        'isImage'
    ];
}
