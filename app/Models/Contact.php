<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Traits\Spatial;
use TCG\Voyager\Traits\Translatable;

class Contact extends Model
{
    use HasFactory, Translatable, SoftDeletes, Spatial;

    protected $fillable = [
        'text1',
        'text2',
        'email',
        'text3',
        'icon1',
        'link1',
        'icon2',
        'link2',
        'icon3',
        'link3',
        'icon4',
        'link4',
        'icon5',
        'link5',
    ];

    protected $translatable = [
        'address',
    ];

    protected $spatial = [
        'map',
    ];

    protected $casts = [
        'phones' => 'array',
        'schedule' => 'array',
    ];

    public function locationCity(): BelongsTo
    {
        return $this->belongsTo(LocationCity::class);
    }

    public function getIcons() : array
    {
        $res = [];

        for($i = 0; $i < 6; $i++){
            $icon = 'icon'.$i;
            if($this->$icon){
                $link = 'link'.$i;
                array_push($res, [
                    "icon" => Storage::disk(config('voyager.storage.disk'))->url($this->$icon),
                    "link" => $this->$link
                ]);
            }
        }
        return $res;
    }
}
