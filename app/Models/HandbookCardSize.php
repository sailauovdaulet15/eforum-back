<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookCardSize
 *
 * @property int $id
 * @property string $name Название карточки
 * @method static Builder|HandbookCardSize newModelQuery()
 * @method static Builder|HandbookCardSize newQuery()
 * @method static Builder|HandbookCardSize query()
 * @method static Builder|HandbookCardSize whereId($value)
 * @method static Builder|HandbookCardSize whereName($value)
 * @property-read collection|Event[] $events
 * @property-read int|null $events_count
 * @mixin \Eloquent
 */
class HandbookCardSize extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $translatable = [
        'name',
    ];

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
