<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Traits\Translatable;

class Faq extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;

    protected $translatable = [
        'title',
        'description',
    ];
}
