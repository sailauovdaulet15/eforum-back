<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class MainMenu extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'label',
        'link',
        'position'
    ];

    public static function boot(): void
    {
        parent::boot();

        self::creating(function(self $mainBanner): void {
            $mainBanner->position = self::max('position') + 1;
        });

        self::deleting(function(self $mainBanner): void {
            $mainBanners = self::where('position', '>', $mainBanner->position)->get();
            foreach ($mainBanners as $banner) {
                $banner->position--;
                $banner->save();
            }
        });
    }
}

