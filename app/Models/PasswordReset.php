<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordReset extends Model
{
    use HasFactory;

    const STATUS_SUCCESS_SENT = 1; //статус успешно отправлен
    const STATUS_PASSWORD_NOT_UPDATED = 2; //статус пароль не обновлен
    const STATUS_PASSWORD_SUCCESS_UPDATED = 3; //статус пароль успешно обновлен

    protected $fillable = [
        'email',
        'status_id',
    ];
}
