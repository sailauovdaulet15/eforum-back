<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Traits\Translatable;

class Organizer extends Model
{
    use HasFactory;

    protected $fillable = [
        'logo',
        'name',
        'event_id',
        'position',
        'handbook_type_sponsor_id'
    ];

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }

    public function getLogo(): ?string
    {
        return Storage::disk('public')->url('/stubs/_thumb_20679.png');
    }
}
