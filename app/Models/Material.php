<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Traits\Translatable;

class Material extends Model
{
    use HasFactory, SoftDeletes, Translatable, PositionTrait;

    protected $translatable = [
        'name',
    ];

    protected $fillable = [
        'name',
        'event_id',
        'file',
        'position'
    ];


    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public static function boot(): void
    {
        parent::boot();

        self::creating(function(self $material): void {
            $material->position = self::max('position') + 1;
        });

        self::deleting(function(self $material): void {
            $materials = self::where('position', '>', $material->position)->get();
            foreach ($materials as $item) {
                $item->position--;
                $item->save();
            }
        });
    }
}
