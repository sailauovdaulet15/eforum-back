<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Traits\Translatable;

class HandbookTypeComplaint extends Model
{
    use HasFactory, SoftDeletes, Translatable, PositionTrait;

    protected $fillable = [
        'name', 'position'
    ];

    protected $translatable = [
        'name',
    ];

    protected $appends = [
        'posit'
    ];

}
