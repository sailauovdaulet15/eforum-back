<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class About extends Model
{
    use HasFactory, Translatable, SoftDeletes;

    protected $fillable = [
        'title',
        'content',
        'image_uri',
        'files',
        'seo_title',
        'seo_description',
        'seo_image',
        'seo_slug'
    ];

    protected $translatable = [
        'title',
        'seo_title',
        'seo_description',
        'seo_slug',
    ];


    protected $casts = [
        'files' => 'array',
    ];

}
