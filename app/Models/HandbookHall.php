<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookHall
 *
 * @property int $id
 * @property string $name Наименование зала
 * @property int $location_city_id Id города
 * @property int $handbook_site_id Id площадка
 * @method static Builder|HandbookHall newModelQuery()
 * @method static Builder|HandbookHall newQuery()
 * @method static Builder|HandbookHall query()
 * @method static Builder|HandbookHall whereId($value)
 * @method static Builder|HandbookHall whereName($value)
 * @mixin \Eloquent
 */
class HandbookHall extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $translatable = [
        'name',
    ];

    public function eventProgram(): HasMany
    {
        return $this->hasMany(EventProgram::class);
    }

    public function locationCity(): BelongsTo
    {
        return $this->belongsTo(LocationCity::class);
    }

    public function handbookSite(): BelongsTo
    {
        return $this->belongsTo(HandbookSite::class);
    }

}
