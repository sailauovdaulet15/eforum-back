<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class LocationCity extends Model
{
    use HasFactory, Translatable, SoftDeletes;

    protected $fillable = [
        'name',
        'location_country_id',
    ];

    protected $translatable = [
        'name',
    ];

    public function locationCountry(): BelongsTo
    {
        return $this->belongsTo(LocationCountry::class);
    }

    public function contacts(): BelongsToMany
    {
        return $this->belongsToMany(Contact::class);
    }
}
