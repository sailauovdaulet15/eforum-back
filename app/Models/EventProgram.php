<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;

/**
 * App\Models\EventProgram
 *
 * @property int $id
 * @property string $date Дата
 * @property string $hall Зал
 * @property string $time_from Время от
 * @property string $time_to Время до
 * @property string $title Название сессии
 * @property string $description Описание сессии
 * @property int $event_id Мероприятия
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Event $event
 * @property-read Collection|Speaker[] $speakers
 * @property-read int|null $speakers_count
 * @method static Builder|EventProgram newModelQuery()
 * @method static Builder|EventProgram newQuery()
 * @method static Builder|EventProgram onlyTrashed()
 * @method static Builder|EventProgram query()
 * @method static Builder|EventProgram whereCreatedAt($value)
 * @method static Builder|EventProgram whereDate($value)
 * @method static Builder|EventProgram whereDeletedAt($value)
 * @method static Builder|EventProgram whereDescription($value)
 * @method static Builder|EventProgram whereHall($value)
 * @method static Builder|EventProgram whereId($value)
 * @method static Builder|EventProgram whereTimeFrom($value)
 * @method static Builder|EventProgram whereTimeUntil($value)
 * @method static Builder|EventProgram whereTitle($value)
 * @method static Builder|EventProgram whereUpdatedAt($value)
 * @method static Builder|EventProgram withTrashed()
 * @method static Builder|EventProgram withoutTrashed()
 * @mixin \Eloquent
 */
class EventProgram extends Model
{
    use HasFactory, PositionTrait;

    protected $fillable = [
        'date',
        'time_from',
        'time_until',
        'title',
        'description',
        'event_id',
        'handbook_hall_id',
        'position'
    ];

    protected $appends = [
        'posit'
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function handbookHall(): BelongsTo
    {
        return $this->belongsTo(HandbookHall::class);
    }

    public function speakerEventPrograms(): HasMany
    {
        return $this->HasMany(SpeakerEventProgram::class, 'event_program_id');
    }

    public function moderators(): HasMany
    {
        return $this->hasMany(SpeakerEventProgram::class)->where('is_moderator', true)
            ->orderBy('position', 'desc');
    }

    public function speakers(): HasMany
    {
        return $this->hasMany(SpeakerEventProgram::class)->where('is_moderator', false)
            ->orderBy('position', 'desc');
    }

    public static function boot(): void
    {
        parent::boot();

        self::creating(function (self $organizerEvent): void {
            $organizerEvent->position = self::where('event_id', $organizerEvent->event_id)->count() + 1;
        });

        self::updating(function (self $organizerEvent): void {
            if (intval($organizerEvent->getOriginal('event_id')) !== intval($organizerEvent->event_id)) {

                self::where('position', '>', intval($organizerEvent->getOriginal('position')))
                    ->where(['event_id' => intval($organizerEvent->getOriginal('event_id'))])
                    ->update(['position' => \DB::raw('position-1')]);

                $organizerEvent->position = self::where(['event_id' => intval($organizerEvent->event_id)])->count() + 1;
            }
        });

    }

    public static function eventIdRelationship($id)
    {
        return
            self::where('event_programs.id', '=', $id)
                ->select('event_programs.event_id', 'events.id as event_id')
                ->join('events', 'event_programs.event_id', '=', 'events.id')
                ->join('handbook_sites', 'events.handbook_site_id', '=', 'handbook_sites.id')
                ->first();
    }

    public function getPositAttribute(): ?int
    {
        return $this->position;
    }
}
