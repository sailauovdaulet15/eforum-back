<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookCategory
 *
 * @property int $id
 * @property string $name Названия категории
 * @method static Builder|HandbookCategory newModelQuery()
 * @method static Builder|HandbookCategory newQuery()
 * @method static Builder|HandbookCategory query()
 * @method static Builder|HandbookCategory whereId($value)
 * @method static Builder|HandbookCategory whereName($value)
 * @mixin \Eloquent
 * @property-read Collection|Event[] $events
 * @property-read int|null $events_count
 */
class HandbookCategory extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $translatable = [
        'name',
    ];

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
