<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Broadcast extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'event_id',
        'link_broadcast',
        'handbook_language_id',
        'handbook_hall_id',
        'date'
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function handbookHall(): BelongsTo
    {
        return $this->belongsTo(HandbookHall::class);
    }

    public function handbookLanguage(): BelongsTo
    {
        return $this->belongsTo(HandbookLanguage::class);
    }
}
