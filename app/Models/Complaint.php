<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Complaint extends Model
{
    use HasFactory;

    protected $fillable = [
        'full_name',
        'email',
        'comment_id',
        'user_id',
        'handbook_type_complaint_id',
        'event_id',
    ];
    public function comment():BelongsTo
    {
        return $this->belongsTo(Comment::class);
    }
    public function user():BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function handbookTypeComplaint():BelongsTo{
        return $this->belongsTo(HandbookTypeComplaint::class);
    }
}
