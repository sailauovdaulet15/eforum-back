<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\HandbookSpeaker
 *
 * @property int $id
 * @property string $full_name ФИО спикера
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|HandbookSpeaker newModelQuery()
 * @method static Builder|HandbookSpeaker newQuery()
 * @method static Builder|HandbookSpeaker query()
 * @method static Builder|HandbookSpeaker whereCreatedAt($value)
 * @method static Builder|HandbookSpeaker whereDeletedAt($value)
 * @method static Builder|HandbookSpeaker whereFullName($value)
 * @method static Builder|HandbookSpeaker whereId($value)
 * @method static Builder|HandbookSpeaker whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HandbookSpeaker extends Model
{
    use HasFactory;
}
