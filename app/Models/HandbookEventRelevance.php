<?php

namespace App\Models;

use Carbon\Carbon;
use Exception;
use http\Message;
use Illuminate\Auth\Events\Failed;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Boolean;
use TCG\Voyager\Traits\Translatable;

class HandbookEventRelevance extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $fillable = [
        'name'
    ];

    protected $translatable = [
        'name',
    ];

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
