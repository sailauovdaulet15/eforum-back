<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;

class SpeakerEvent extends Model
{
    use HasFactory, SoftDeletes, PositionTrait;

    protected $fillable = [
        'speaker_id',
        'event_id',
        'is_moderator',
        'position'
    ];

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function speaker(): BelongsTo
    {
        return $this->belongsTo(Speaker::class);
    }

    public static function boot(): void
    {

        parent::boot();
        self::creating(function(self $speakerEvent): void {
            $speakerEvent->position = self::where('event_id', $speakerEvent->event_id)->count() + 1;
        });

        self::updating(function(self $speakerEvent):void {
            if(intval($speakerEvent->getOriginal('event_id'))!==intval($speakerEvent->event_id)){

                self::where('position', '>', intval($speakerEvent->getOriginal('position')))
                    ->where(['event_id' => intval($speakerEvent->getOriginal('event_id'))])
                    ->update(['position' => \DB::raw('position-1')]);

                $speakerEvent->position = self::where(['event_id' => intval($speakerEvent->event_id)])->count()+1;
            }
        });

    }

    public function getEventProgramAttribute()
    {
        $eventProgram = $this->event->eventPrograms()->orderBy('position', 'desc')->first();
        if($eventProgram)
        {
            return $eventProgram->title;
        }
        return null;
    }

    public function getPhoto(): ?string
    {
        if (!$this->speaker) {
            return null;
        } elseif ($this->speaker->photo != null) {
           return Storage::disk(config('voyager.storage.disk'))->url($this->speaker->photo);
        } elseif ($this->speaker->photo == null) {
          return Storage::disk('public')->url('/stubs/_thumb_20676.png');
        }
    }

}
