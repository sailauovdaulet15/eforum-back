<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'full_name',
        'email',
        'parent_id',
        'content',
        'event_id',
        'user_id',
        'is_active'
        ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $with = ['child'];



    public function user():BelongsTo{
        return $this->belongsTo(User::class);
    }

    public function parent():BelongsTo{
        return $this->belongsTo(Comment::class,'parent_id', 'id');
    }
    public function event():BelongsTo{
        return $this->belongsTo(Event::class);
    }
   public function child():HasMany
   {
        return $this->hasMany(Static::class,'parent_id')->orderBy('id', 'desc');
   }

   public function getAvatar():string
   {
       return $this->user_id ? $this->user->getAvatar()
           : Storage::disk('public')->url('/stubs/_thumb_20676.png');
   }
}
