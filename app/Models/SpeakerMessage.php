<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpeakerMessage extends Model
{

    use HasFactory;

    protected $fillable = [
        'sender_id',
        'recipient_id',
        'text',
        'parent_id',
        'is_active'
    ];

    public function sender():BelongsTo
    {
       return $this->belongsTo(Speaker::class, 'sender_id');
    }
    public function recipient():BelongsTo
    {
        return $this->belongsTo(Speaker::class, 'recipient_id');
    }

    public function child(): HasOne
    {
        return $this->hasOne(Static::class, 'parent_id');
    }

    public function childs(): HasMany
    {
        return $this->hasMany(Static::class, 'parent_id')->orderBy('id', 'desc');
    }
}
