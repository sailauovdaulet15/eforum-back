<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Models\Translation;


/**
 * App\Models\Speaker
 *
 * @property int $id
 * @property string $full_name ФИО
 * @property string $photo Фото
 * @property string $about_speaker О спикере
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read null $translated
 * @property-read Collection|Translation[] $translations
 * @property-read int|null $translations_count
 * @method static Builder|Speaker newModelQuery()
 * @method static Builder|Speaker newQuery()
 * @method static Builder|Speaker onlyTrashed()
 * @method static Builder|Speaker query()
 * @method static Builder|Speaker whereAboutSpeaker($value)
 * @method static Builder|Speaker whereCreatedAt($value)
 * @method static Builder|Speaker whereDeletedAt($value)
 * @method static Builder|Speaker whereEventId($value)
 * @method static Builder|Speaker whereFullName($value)
 * @method static Builder|Speaker whereId($value)
 * @method static Builder|Speaker wherePhoto($value)
 * @method static Builder|Speaker whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|Speaker whereUpdatedAt($value)
 * @method static Builder|Speaker withTranslation($locale = null, $fallback = true)
 * @method static Builder|Speaker withTranslations($locales = null, $fallback = true)
 * @method static Builder|Speaker withTrashed()
 * @method static Builder|Speaker withoutTrashed()
 * @mixin \Eloquent
 */
class Speaker extends Model
{
    use HasFactory;

    protected $fillable = [
        'photo',
        'full_name',
        'about_speaker',
    ];

    public function speakerEvents(): HasMany
    {
        return $this->hasMany(SpeakerEvent::class);
    }

    public function speakerEventPrograms(): HasMany
    {
        return $this->hasMany(SpeakerEventProgram::class);
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }
    public static function getAboutSpeaker($jsonEncode = true){

        $items = Speaker::all()->toArray();
        $result = [];
        array_push($result, [
            'id' => 0,
            'name' => 'не выбрано',
            'children' => [],
        ]);
        foreach ($items as $item) {
            $json = Speaker::select('about_speaker')->findOrFail($item['id']);
            $json = json_decode($json->about_speaker, true);
            if ($json && is_array($json)) {
                array_unshift($json['rows'], "");
                unset($json['rows'][0]);
                $count = count($json['rows']);
                $temp = [];
                array_push($temp, [
                    'id' => 0,
                    'name' => 'не выбрано',
                ]);
                for ($i = 1; $i <= $count; $i++) {
                    array_push($temp,
                        [
                            'id' => array_values($json['rows'][$i]),
                            'name' => array_values($json['rows'][$i])
                        ]);
                }
                array_push($result, [
                    'id' => $item['id'],
                    'name' => $item['full_name'],
                    'children' => $temp,
                ]);
            } else {
                array_push($result, [
                    'id' => $item['id'],
                    'name' => $item['full_name'],
                    'children' => [],
                ]);
            }


        }
        return $jsonEncode ? json_encode($result) : $result;
    }

    public static function changeJson($data): array
    {
        if ($data){
            $data = json_decode($data, true);
            if ($data['rows']){
                return $data['rows'];
            }
        }

        return [];
    }

}
