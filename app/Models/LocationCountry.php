<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class LocationCountry extends Model
{
    use HasFactory, Translatable, SoftDeletes;

    protected $fillable = [
        'name',
        'iso_code',
    ];

    protected $translatable = [
        'name',
    ];

    public function locationCities(): HasMany
    {
        return $this->hasMany(LocationCity::class);
    }
}
