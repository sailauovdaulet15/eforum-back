<?php

namespace App\Models;

use App\Http\Resources\SpeakerResource;
use App\Http\Resources\UserResource;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends \TCG\Voyager\Models\User implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'email_verified_at',
        'password',
        'avatar',
        'full_name',
        'phone',
        'company',
        'post',
        'location_country_id',
        'location_city_id',
        'account_is_active',
        'date_time_last_login',
        'is_notification_events',
        'speaker_id',
        'isSpeaker',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function locationCountry(): BelongsTo
    {
        return $this->belongsTo(LocationCountry::class);
    }

    public function locationCity(): BelongsTo
    {
        return $this->belongsTo(LocationCity::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function speaker(): BelongsTo
    {
        return $this->belongsTo(Speaker::class);
    }

    public function getSpeaker(): ?SpeakerResource
    {
        if ($this->isSpeaker == true) {
            return new SpeakerResource($this->speaker);
        }
        return null;
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function getAvatar(): string
    {
        if ($this->isSpeaker && $this->speaker->photo) {
            return Storage::disk(config('voyager.storage.disk'))->url($this->speaker->photo);
        }
        if ($this->avatar == 'users/default.png') {
            return Storage::disk('public')->url('/stubs/_thumb_20676.png');
        } elseif ($this->avatar == null) {
            return Storage::disk('public')->url('/stubs/_thumb_20676.png');
        } else {
            return Storage::disk(config('voyager.storage.disk'))->url($this->avatar);
        }
    }
}
