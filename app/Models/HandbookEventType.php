<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookEventType
 *
 * @property int $id
 * @property string $name Название типа
 * @method static Builder|HandbookEventType newModelQuery()
 * @method static Builder|HandbookEventType newQuery()
 * @method static Builder|HandbookEventType query()
 * @method static Builder|HandbookEventType whereId($value)
 * @method static Builder|HandbookEventType whereName($value)
 * @property-read collection|Event[] $events
 * @property-read int|null $events_count
 * @mixin \Eloquent
 */
class HandbookEventType extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $translatable = [
        'name',
    ];


    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }
}
