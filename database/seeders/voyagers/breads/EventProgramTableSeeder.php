<?php

namespace Database\Seeders\voyagers\breads;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class EventProgramTableSeeder extends Seeder
{
    private const TABLE_NAME = 'event_programs';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Программа мероприятия',
            'display_name_plural' => 'Программа мероприятия',
            'icon' => '',
            'model_name' => 'App\\Models\\EventProgram',
            'controller' => '',
            'generate_permissions' => 1,
            'description' => '',
//            'server_side' => 1,
            'details' => [
                'order_column' => 'position',
                'order_display_column' => 'event_id',
                'order_group' => ['event_id']
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'Id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field'        => 'handbook_hall_id',
            'type'         => 'select_dropdown',
            'display_name' => 'Зал',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'details'      => [
                'default' => '',
                'null'    => '',
                'options' => [
                    '' => '-- Не выбрано --',
                ],
                'relationship' => [
                    'key'   => 'id',
                    'label' => 'name',
                ],

            ],
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'handbook_hall_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Зал',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\HandbookHall',
                'table' => 'handbook_halls',
                'type' => 'belongsTo',
                'column' => 'handbook_hall_id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ],
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field'        => 'event_id',
            'type'         => 'select_dropdown',
            'display_name' => 'Мероприятие',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'details'      => [
                'default' => '',
                'null'    => '',
                'options' => [
                    '' => '-- Не выбрано --',
                ],
                'relationship' => [
                    'key'   => 'id',
                    'label' => 'title',
                ],

            ],
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'event_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Мероприятие',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\Event',
                'table' => 'events',
                'type' => 'belongsTo',
                'column' => 'event_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'date',
            'type' => 'date',
            'display_name' => 'Дата',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'format' => '%d-%m-%Y'
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'time_from',
            'type' => 'time',
            'display_name' => 'Время от',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'time_to',
            'type' => 'time',
            'display_name' => 'Время до',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'title',
            'type' => 'text',
            'display_name' => 'Название сессии',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'description',
            'type' => 'rich_text_box',
            'display_name' => 'Описание сесии',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'position',
            'type' => 'number',
            'display_name' => 'Позиция',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата изменения',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0
        ]);

        Permission::generateFor(self::TABLE_NAME);
    }
}
