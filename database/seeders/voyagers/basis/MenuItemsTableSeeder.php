<?php

namespace Database\Seeders\voyagers\basis;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;

class MenuItemsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'admin')->firstOrFail();


        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Мероприятия',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-ticket',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Мероприятия',
            'url' => '',
            'route' => 'voyager.events.index',
            'target' => '_self',
            'icon_class' => 'voyager-ticket',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Организаторы',
            'url' => '',
            'route' => 'voyager.organizers.index',
            'target' => '_self',
            'icon_class' => 'voyager-archive',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Организаторы мероприятий',
            'url' => '',
            'route' => 'voyager.organizer_events.index',
            'target' => '_self',
            'icon_class' => 'voyager-categories',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Программа мероприятия',
            'url' => '',
            'route' => 'voyager.event_programs.index',
            'target' => '_self',
            'icon_class' => 'voyager-documentation',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Материалы и видео',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-ticket',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Материалы',
            'url' => '',
            'route' => 'voyager.materials.index',
            'target' => '_self',
            'icon_class' => 'voyager-receipt',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Трансляции',
            'url' => '',
            'route' => 'voyager.broadcasts.index',
            'target' => '_self',
            'icon_class' => 'voyager-youtube-play',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Комментарии',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-bubble',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Комментарии',
            'url' => '',
            'route' => 'voyager.comments.index',
            'target' => '_self',
            'icon_class' => 'voyager-file-text',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Жалобы',
            'url' => '',
            'route' => 'voyager.complaints.index',
            'target' => '_self',
            'icon_class' => 'voyager-documentation',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Пользователи',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-group',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Пользователи',
            'url' => '',
            'route' => 'voyager.users.index',
            'target' => '_self',
            'icon_class' => 'voyager-group',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Сообщения спикеров',
            'url' => '',
            'route' => 'voyager.speaker_messages.index',
            'target' => '_self',
            'icon_class' => 'voyager-group',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Заявки',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-thumb-tack',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Заявки на мероприятия',
            'url' => '',
            'route' => 'voyager.event_statements.index',
            'target' => '_self',
            'icon_class' => 'voyager-mail',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Обратная связь',
            'url' => '',
            'route' => 'voyager.feedback.index',
            'target' => '_self',
            'icon_class' => 'voyager-window-list',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Спикеры',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-ticket',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Спикеры',
            'url' => '',
            'route' => 'voyager.speakers.index',
            'target' => '_self',
            'icon_class' => 'voyager-people',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Спикеры мероприятий',
            'url' => '',
            'route' => 'voyager.speaker_events.index',
            'target' => '_self',
            'icon_class' => 'voyager-people',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Спикеры сессии',
            'url' => '',
            'route' => 'voyager.speaker_event_programs.index',
            'target' => '_self',
            'icon_class' => 'voyager-people',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Справочники',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Категория мероприятия',
            'url' => '',
            'route' => 'voyager.handbook_categories.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Залы',
            'url' => '',
            'route' => 'voyager.handbook_halls.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Тип спонсора',
            'url' => '',
            'route' => 'voyager.handbook_type_sponsors.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Тип мероприятия',
            'url' => '',
            'route' => 'voyager.handbook_event_types.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Актуальность',
            'url' => '',
            'route' => 'voyager.handbook_event_relevances.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Вид мероприятия',
            'url' => '',
            'route' => 'voyager.handbook_event_kinds.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Тип жалоб',
            'url' => '',
            'route' => 'voyager.handbook_type_complaints.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Язык',
            'url' => '',
            'route' => 'voyager.handbook_languages.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Площадка',
            'url' => '',
            'route' => 'voyager.handbook_sites.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Страны',
            'url' => '',
            'route' => 'voyager.location_countries.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Города',
            'url' => '',
            'route' => 'voyager.location_cities.index',
            'target' => '_self',
            'icon_class' => 'voyager-book',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Другое',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-documentation',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Новости',
            'url' => '',
            'route' => 'voyager.news.index',
            'target' => '_self',
            'icon_class' => 'voyager-news',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Контакты',
            'url' => '',
            'route' => 'voyager.contacts.index',
            'target' => '_self',
            'icon_class' => 'voyager-world',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Главная страница',
            'url' => '',
            'route' => 'voyager.main_pages.index',
            'target' => '_self',
            'icon_class' => 'voyager-browser',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Меню',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-treasure',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Основное меню',
            'url' => '',
            'route' => 'voyager.main_menus.index',
            'target' => '_self',
            'icon_class' => 'voyager-window-list',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Футер',
            'url' => '',
            'route' => 'voyager.footers.index',
            'target' => '_self',
            'icon_class' => 'voyager-window-list',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'О проекте',
            'url' => '',
            'route' => 'voyager.abouts.index',
            'target' => '_self',
            'icon_class' => 'voyager-file-text',
            'color' => null,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Настройки',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-params',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Проект',
            'url' => '',
            'route' => 'voyager.settings.index',
            'target' => '_self',
            'icon_class' => 'voyager-settings',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'SEO',
            'url' => '',
            'route' => 'voyager.seo_pages.index',
            'target' => '_self',
            'icon_class' => 'voyager-wand',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Роли',
            'url' => '',
            'route' => 'voyager.roles.index',
            'target' => '_self',
            'icon_class' => 'voyager-lock',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Медиа',
            'url' => '',
            'route' => 'voyager.media.index',
            'target' => '_self',
            'icon_class' => 'voyager-images',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Инструменты',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-tools',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Menu Builder',
            'url' => '',
            'route' => 'voyager.menus.index',
            'target' => '_self',
            'icon_class' => 'voyager-list',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Database',
            'url' => '',
            'route' => 'voyager.database.index',
            'target' => '_self',
            'icon_class' => 'voyager-data',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Compass',
            'url' => '',
            'route' => 'voyager.compass.index',
            'target' => '_self',
            'icon_class' => 'voyager-compass',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Bread',
            'url' => '',
            'route' => 'voyager.bread.index',
            'target' => '_self',
            'icon_class' => 'voyager-bread',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);
    }
}
