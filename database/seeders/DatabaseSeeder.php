<?php

namespace Database\Seeders;

use Database\Seeders\voyagers\basis\RolesTableSeeder;
use Database\Seeders\voyagers\basis\MenusTableSeeder;
use Database\Seeders\voyagers\basis\MenuItemsTableSeeder;
use Database\Seeders\voyagers\basis\SettingsTableSeeder;
use Database\Seeders\voyagers\basis\PermissionsTableSeeder;
use Database\Seeders\voyagers\basis\PermissionRoleSeeder;
use Database\Seeders\voyagers\breads\BroadCastTableSeeder;
use Database\Seeders\voyagers\breads\CommentsTableSeeder;
use Database\Seeders\voyagers\breads\ComplaintTableSeeder;
use Database\Seeders\voyagers\breads\HandbookCategoryTableSeeder;
use Database\Seeders\voyagers\breads\ContactTableSeeder;
use Database\Seeders\voyagers\breads\HandbookEventKindTableSeeder;
use Database\Seeders\voyagers\breads\EventProgramTableSeeder;
use Database\Seeders\voyagers\breads\EventStatementTableSeeder;
use Database\Seeders\voyagers\breads\HandbookEventRelevanceTableSeeder;
use Database\Seeders\voyagers\breads\HandbookEventStatusTableSeeder;
use Database\Seeders\voyagers\breads\EventTableSeeder;
use Database\Seeders\voyagers\breads\FaqTableSeeder;
use Database\Seeders\voyagers\breads\HandbookEventTypeTableSeeder;
use Database\Seeders\voyagers\breads\HandbookHallTableSeeder;
use Database\Seeders\voyagers\breads\HandbookLanguageTableSeeder;
use Database\Seeders\voyagers\breads\HandbookSiteTableSeeder;
use Database\Seeders\voyagers\breads\HandbookTypeComplaintTableSeeder;
use Database\Seeders\voyagers\breads\HandbookTypeSponsorTableSeeder;
use Database\Seeders\voyagers\breads\MainPageTableSeeder;
use Database\Seeders\voyagers\breads\MaterialTableSeeder;
use Database\Seeders\voyagers\breads\NewsTableSeeder;
use Database\Seeders\voyagers\breads\FeedbackTableSeeder;
use Database\Seeders\voyagers\breads\MainMenuTableSeeder;
use Database\Seeders\voyagers\breads\HandbookNewsTagTableSeeder;
use Database\Seeders\voyagers\breads\HandbookRequestSubjectTableSeeder;
use Database\Seeders\voyagers\breads\LocationCityTableSeeder;
use Database\Seeders\voyagers\breads\LocationCountryTableSeeder;
use Database\Seeders\voyagers\breads\OrganizerEventTableSeeder;
use Database\Seeders\voyagers\breads\OrganizerTableSeeder;
use Database\Seeders\voyagers\breads\AboutTableSeeder;
use Database\Seeders\voyagers\breads\SeoPageTableSeeder;
use Database\Seeders\voyagers\breads\SpeakerEventProgramTableSeeder;
use Database\Seeders\voyagers\breads\SpeakerEventTableSeeder;
use Database\Seeders\voyagers\breads\SpeakerMessagesTableSeeder;
use Database\Seeders\voyagers\breads\SpeakerTableSeeder;
use Database\Seeders\voyagers\breads\FooterTableSeeder;
use Database\Seeders\voyagers\breads\UserTableSeeder;
use Database\Seeders\voyagers\breads\VideoRecordTableSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateVoyagerSeed();
        $this->callVoyagerBreadTables();
        $this->callVoyagerBasicTables();
        $this->callDataFakes();
        $this->callDataOriginals();
    }

    /**
     * Очистка старых данных для voyager.
     */
    private function truncateVoyagerSeed()
    {
        DB::table('menu_items')->truncate();
        DB::table('settings')->truncate();
        DB::table('permissions')->truncate();
        DB::table('permission_role')->truncate();
        DB::table('menus')->truncate();
        DB::table('data_rows')->truncate();
        DB::table('data_types')->truncate();
    }

    /**
     * Вызов объектов по namespace "\Database\Seeders\voyagers\breads"
     */
    private function callVoyagerBreadTables()
    {
        $this->call(ContactTableSeeder::class);
        $this->call(FeedbackTableSeeder::class);
        $this->call(FaqTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(LocationCountryTableSeeder::class);
        $this->call(LocationCityTableSeeder::class);
        $this->call(HandbookRequestSubjectTableSeeder::class);
        $this->call(SeoPageTableSeeder::class);
        $this->call(AboutTableSeeder::class);
        $this->call(HandbookCategoryTableSeeder::class);
        $this->call(HandbookEventKindTableSeeder::class);
        $this->call(HandbookEventStatusTableSeeder::class);
        $this->call(EventTableSeeder::class);
        $this->call(SpeakerTableSeeder::class);
        $this->call(EventStatementTableSeeder::class);
        $this->call(EventProgramTableSeeder::class);
        $this->call(HandbookHallTableSeeder::class);
        $this->call(MainPageTableSeeder::class);
        $this->call(OrganizerTableSeeder::class);
        $this->call(MaterialTableSeeder::class);
        $this->call(SpeakerEventProgramTableSeeder::class);
        $this->call(SpeakerEventTableSeeder::class);
        $this->call(HandbookTypeSponsorTableSeeder::class);
        $this->call(HandbookEventTypeTableSeeder::class);
        $this->call(HandbookEventRelevanceTableSeeder::class);
        $this->call(HandbookTypeComplaintTableSeeder::class);
        $this->call(HandbookLanguageTableSeeder::class);
        $this->call(BroadCastTableSeeder::class);
        $this->call(HandbookSiteTableSeeder::class);
        $this->call(MainMenuTableSeeder::class);
        $this->call(OrganizerEventTableSeeder::class);
        $this->call(ComplaintTableSeeder::class);
        $this->call(FooterTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(VideoRecordTableSeeder::class);
        $this->call(SpeakerMessagesTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
    }

    /**
     * Вызов объектов по namespace "Database\Seeders\voyagers\basis"
     */
    private function callVoyagerBasicTables()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(MenuItemsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }

    /**
     * Вызов объектов по namespace "\Database\Seeders\data\fakes"
     */
    private function callDataFakes()
    {
//        $this->call(\Database\Seeders\data\fakes\FeedbackTableSeeder::class);
//        $this->call(\Database\Seeders\data\fakes\SeoPageTableSeeder::class);
    }

    /**
     * Вызов объектов по namespace "\Database\Seeders\data\originals"
     */
    private function callDataOriginals()
    {
        $this->call(\Database\Seeders\data\originals\HandbookEventStatusTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookEventKindTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookEventTypeTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookEventRelevanceTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookCategoryTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookTypeComplaintTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookTypeSponsorTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookLanguageTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookLocationCountryTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookLocationCityTableSeeder::class);
        $this->call(\Database\Seeders\data\originals\HandbookSiteTableSeeder::class);    }
}
