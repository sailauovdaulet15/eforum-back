<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookEventStatus;
use Illuminate\Database\Seeder;

class HandbookEventStatusTableSeeder extends Seeder
{
    public function run()
    {
        if (HandbookEventStatus::all()->count() === 0) {
            HandbookEventStatus::upsert([
                ['name' => 'Платный'],
                ['name' => 'Бесплатный'],
            ], ['name']);
        }
    }
}
