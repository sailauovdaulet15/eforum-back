<?php

namespace Database\Seeders\data\originals;

use App\Models\LocationCity;
use App\Models\LocationCountry;
use Illuminate\Database\Seeder;

class HandbookLocationCityTableSeeder extends Seeder
{
    public function run()
    {
        LocationCity::firstOrNew([
            'name' => 'Нур-Султан',
            'location_country_id' => LocationCountry::firstWhere('name', 'Казахстан')->id,
        ])->save();
    }
}
