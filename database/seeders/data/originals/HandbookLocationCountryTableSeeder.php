<?php

namespace Database\Seeders\data\originals;

use App\Models\LocationCountry;
use Illuminate\Database\Seeder;

class HandbookLocationCountryTableSeeder extends Seeder
{
    public function run()
    {
        LocationCountry::firstOrNew([
            'name' => 'Казахстан',
            'iso_code' => 'kz',
        ])->save();
    }
}
