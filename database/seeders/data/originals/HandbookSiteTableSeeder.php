<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookSite;
use App\Models\LocationCity;
use Illuminate\Database\Seeder;

class HandbookSiteTableSeeder extends Seeder
{
    public function run()
    {
        if(HandbookSite::all()->count() === 0){
            $locations = [
                ['name' => 'Дворец Независимости - Тәуелсіздік Сарайы',
                    'location_city_id' => LocationCity::firstWhere('name','Нур-Султан')->id],
                ['name' => 'Конгресс-центр EXPO',  'location_city_id' => LocationCity::firstWhere('name','Нур-Султан')->id],
                ['name' => 'Барыс Арена', 'location_city_id' => LocationCity::firstWhere('name','Нур-Султан')->id],
                ['name' => 'Дворец мира и согласия', 'location_city_id' => LocationCity::firstWhere('name','Нур-Султан')->id]
            ];
            foreach ($locations as $location) {
                HandbookSite::firstOrCreate($location);
            }
        }
    }
}
