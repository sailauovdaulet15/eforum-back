<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookEventRelevance;
use Illuminate\Database\Seeder;

class HandbookEventRelevanceTableSeeder extends Seeder
{
    public function run()
    {
        if (HandbookEventRelevance::all()->count() === 0) {
            HandbookEventRelevance::upsert([
                ['name' => 'Действующее'],
                ['name' => 'Запланированное'],
                ['name' => 'Завершенное']
            ], ['name']);
        }
    }
}
