<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookTypeComplaint;
use Illuminate\Database\Seeder;

class HandbookTypeComplaintTableSeeder extends Seeder
{
    public function run()
    {
        if(HandbookTypeComplaint::all()->count() === 0){
            HandbookTypeComplaint::upsert([
                ['name' => 'Комментарий оскорбителен'],
                ['name' => 'Комментарий не по теме (флуд)'],
                ['name' => 'Реклама вместо комментариев (спам)'],
                ['name' => 'Другое'],
            ], ['name']);
        }

    }
}
