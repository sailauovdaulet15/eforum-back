<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookLanguage;
use App\Models\HandbookTypeSponsor;
use Illuminate\Database\Seeder;

class HandbookLanguageTableSeeder extends Seeder
{
    public function run()
    {
        if (HandbookLanguage::all()->count() === 0) {
            HandbookLanguage::upsert([
                ['name' => 'Русский '],
                ['name' => 'Казахский'],
                ['name' => 'Английский']
            ], ['name']);
        }
    }
}
