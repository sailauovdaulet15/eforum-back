<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookEventKind;
use Illuminate\Database\Seeder;

class HandbookEventKindTableSeeder extends Seeder
{
    public function run()
    {
        if (HandbookEventKind::all()->count() === 0) {
            HandbookEventKind::upsert([
                ['name' => 'Открытое'],
                ['name' => 'Закрытое']
            ], ['name']);
        }
    }
}
