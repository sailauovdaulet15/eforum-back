<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookCategory;
use Illuminate\Database\Seeder;

class HandbookCategoryTableSeeder extends Seeder
{
    public function run()
    {
        if (HandbookCategory::all()->count() === 0) {
            HandbookCategory::upsert([
                ['name' => 'Бизнес'],
                ['name' => 'Обучение'],
                ['name' => 'Финансы']
            ], ['name']);
        }
    }
}
