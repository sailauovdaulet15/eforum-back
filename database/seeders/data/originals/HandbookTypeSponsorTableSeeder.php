<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookTypeComplaint;
use App\Models\HandbookTypeSponsor;
use Illuminate\Database\Seeder;

class HandbookTypeSponsorTableSeeder extends Seeder
{
    public function run()
    {
        if(HandbookTypeSponsor::all()->count() === 0) {
            HandbookTypeSponsor::upsert([
                ['name' => 'Спонсор стратегической сессии'],
                ['name' => 'Технический партнер'],
                ['name' => 'Технический партнер и организатор онлайн-трансляции'],
                ['name' => 'Партнер'],
                ['name' => 'Спонсор тематической сессии 1'],
                ['name' => 'Спонсор тематической сессии 2'],
                ['name' => 'Спонсор тематической сессии 3'],
                ['name' => 'Спонсор тематической сессии 4'],
                ['name' => 'Спонсор тематической сессии 5'],
                ['name' => 'Спонсор тематической сессии 6'],
            ], ['name']);
        }
    }
}
