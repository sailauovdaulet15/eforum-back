<?php

namespace Database\Seeders\data\originals;

use App\Models\HandbookEventType;
use Illuminate\Database\Seeder;

class HandbookEventTypeTableSeeder extends Seeder
{
    public function run()
    {
        if (HandbookEventType::all()->count() === 0) {
            $eventTypes = [
                ['name' => 'Онлайн', 'display' => 'online'],
                ['name' => 'Оффлайн', 'display' => 'offline'],
                ['name' => 'Онлайн и Оффлайн', 'display' => 'hybrid'],
            ];
            foreach ($eventTypes as $eventType) {
                HandbookEventType::firstOrCreate($eventType);
            }
        }
    }
}
