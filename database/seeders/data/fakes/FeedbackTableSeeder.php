<?php

namespace Database\Seeders\data\fakes;

use App\Constants\LanguageConstant;
use App\Models\Feedback;
use App\Models\SeoPage;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Translation;

class FeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Feedback::firstOrNew([
            'fio' => 'example',
            'phone' => '+77779997799',
            'email' => 'example@rocketfirm.net',
            'message' => 'example',
        ])->save();

        Feedback::firstOrNew([
            'fio' => 'example_two',
            'phone' => '+77779997799',
            'email' => 'example@rocketfirm.net',
            'message' => 'example',
        ])->save();
    }
}
