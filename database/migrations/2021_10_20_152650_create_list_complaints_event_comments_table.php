<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListComplaintsEventCommentsTable extends Migration
{
    private const TABLE_NAME = 'list_complaints_event_comments';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_comment_id')->comment('Комментарий')
                ->constrained('event_comments')
                ->cascadeOnDelete();
            $table->foreignId('handbook_type_complaint_id')->comment('Тип жалоб')
                ->constrained('handbook_type_complaints')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
