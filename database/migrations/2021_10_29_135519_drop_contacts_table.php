<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropContactsTable extends Migration
{
    private const TABLE_NAME = 'contacts';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists(self::TABLE_NAME);

        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string('text1')->comment('Текст 1');
            $table->string('text2')->comment('Текст 2');
            $table->string('email')->comment('E-mail');
            $table->string('text3')->nullable()->comment('Текст 3');
            $table->string('icon1')->nullable()->comment('Иконка 1');
            $table->string('link1')->nullable()->comment('Ссылка 1');
            $table->string('icon2')->nullable()->comment('Иконка 2');
            $table->string('link2')->nullable()->comment('Ссылка 2');
            $table->string('icon3')->nullable()->comment('Иконка 3');
            $table->string('link3')->nullable()->comment('Ссылка 3');
            $table->string('icon4')->nullable()->comment('Иконка 4');
            $table->string('link4')->nullable()->comment('Ссылка 4');
            $table->string('icon5')->nullable()->comment('Иконка 5');
            $table->string('link5')->nullable()->comment('Ссылка 5');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
