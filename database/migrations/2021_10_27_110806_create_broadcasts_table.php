<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBroadcastsTable extends Migration
{
    private const TABLE_NAME = 'broadcasts';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->comment('Мероприятие')
                ->constrained('events')
                ->cascadeOnDelete();
            $table->string('link_broadcast')->comment('Ссылка на трансляцию (ютуб)');
            $table->foreignId('handbook_language_id')->comment('Язык трансляции')
                ->constrained('handbook_languages')
                ->cascadeOnDelete();
            $table->foreignId('handbook_hall_id')->comment('Зал')
                ->constrained('handbook_halls')
                ->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
