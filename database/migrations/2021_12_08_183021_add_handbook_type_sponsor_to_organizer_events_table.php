<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddHandbookTypeSponsorToOrganizerEventsTable extends Migration
{
    private const TABLE_NAME = 'organizer_events';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->foreignId('handbook_type_sponsor_id')->nullable()
                ->comment('Тип спонсора')
                ->constrained('handbook_type_sponsors')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropConstrainedForeignId('handbook_type_sponsor_id');
        });
    }
}
