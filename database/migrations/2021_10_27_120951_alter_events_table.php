<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterEventsTable extends Migration
{
    private const TABLE_NAME = 'events';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('additional_image');
            $table->string('main_image')->nullable()->change();
            $table->text('description')->change();
            $table->text('short_description')->change();
            $table->foreignId('location_country_id')->comment('Страна')
                ->constrained('location_countries')
                ->cascadeOnDelete();
            $table->foreignId('location_city_id')->comment('Город')
                ->constrained('location_cities')
                ->cascadeOnDelete();
            $table->foreignId('handbook_location_id')->comment('Локация')
                ->nullable()
                ->constrained('handbook_locations')
                ->cascadeOnDelete();
            $table->string('program')->nullable();
            $table->boolean('main_image_is_slider')->default(0)->comment('Главного фото добавить на слайдер');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->string('additional_image');
            $table->string('main_image')->nullable(false)->change();
            $table->dropConstrainedForeignId('location_country_id');
            $table->dropConstrainedForeignId('location_city_id');
            $table->dropConstrainedForeignId('handbook_location_id');
            $table->dropColumn('program');
            $table->dropColumn('main_image_is_slider');
        });
    }
}
