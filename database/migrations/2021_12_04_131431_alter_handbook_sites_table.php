<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterHandbookSitesTable extends Migration
{
    private const TABLE_NAME = 'handbook_sites';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->text('address')->nullable()->comment('Адрес');
            $table->float('latitude', 10,6)->nullable()->comment('Ширина');
            $table->float('longitude', 10,6)->nullable()->comment('Долгота');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
}
