<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintsTable extends Migration
{
    private const TABLE_NAME = 'complaints';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->comment('ФИО');
            $table->string('email')->comment('Email');
            $table->foreignId('event_id')->comment('Мероприятие')
                ->constrained('events')
                ->cascadeOnDelete();
            $table->foreignId('event_comment_id')->comment('Комментарий')
                ->constrained('event_comments')
                ->cascadeOnDelete();
            $table->foreignId('handbook_type_complaint_id')->comment('Вид жалобы')
                ->constrained('handbook_type_complaints')
                ->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
