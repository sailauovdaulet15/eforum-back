<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_programs', function (Blueprint $table) {
            $table->id();
            $table->date('date')->comment('Дата');
            $table->text('hall')->comment('Зал');
            $table->time('time_from')->comment('Время от');
            $table->time('time_to')->comment('Время до');
            $table->string('title')->comment('Заголовок');
            $table->string('description')->comment('Описание');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_programs');
    }
}
