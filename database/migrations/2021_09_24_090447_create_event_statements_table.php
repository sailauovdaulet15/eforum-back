<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_statements', function (Blueprint $table) {
            $table->id();
            $table->string('user_name')->comment('Имя');
            $table->string('surname')->comment('Фамилия');
            $table->foreignId('event_id')->comment('Мероприятия')
                ->constrained('events')
                ->cascadeOnDelete();
            $table->string('phone')->unique()->comment('Номер телефон');
            $table->string('email')->unique()->comment('Адрес эл. почты');
            $table->string('position')->nullable()->comment('Должность');
            $table->string('organization')->nullable()->comment('Организация');
            $table->foreignId('location_country_id')->comment('Страна')
                ->constrained('location_countries')
                ->cascadeOnDelete();
            $table->foreignId('location_city_id')->comment('Город')
                ->constrained('location_cities')
                ->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_statements');
    }
}
