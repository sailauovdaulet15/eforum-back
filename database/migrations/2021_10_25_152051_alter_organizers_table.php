<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOrganizersTable extends Migration
{
    private const TABLE_NAME = 'organizers';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('position');
            $table->dropConstrainedForeignId('event_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->integer('position')->default(0)->comment('Позиция');
            $table->foreignId('event_id')->comment('Мероприятия')
                ->constrained('events')
                ->cascadeOnDelete();
        });
    }
}
