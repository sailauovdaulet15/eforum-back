<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPasswordResetsTable extends Migration
{
    private const TABLE_NAME = 'password_resets';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn(self::TABLE_NAME, 'token')) {

            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->dropColumn('token');
            });
        }
        if (Schema::hasColumn(self::TABLE_NAME, 'created_at')) {

            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->dropColumn('created_at');
            });
        }
        if (!Schema::hasColumns(self::TABLE_NAME, ['status_id', 'id', 'created_at', 'updated_at'])) {
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->integer('status_id')->after('id');
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->integer('id')->unsigned()->change();
            $table->dropColumn('id');
            $table->dropColumn('status_id');
            $table->dropColumn(['created_at', 'updated_at']);
        });
    }
}
