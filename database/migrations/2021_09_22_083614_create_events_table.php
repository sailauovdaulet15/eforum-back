<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('Заголовок');
            $table->foreignId('handbook_category_id')
                ->comment('Категория')
                ->constrained('handbook_categories')
                ->cascadeOnDelete();
            $table->foreignId('handbook_event_kind_id')
                ->comment('Вид')
                ->constrained('handbook_event_kinds')
                ->cascadeOnDelete();
            $table->foreignId('handbook_event_status_id')
                ->comment('Статус')
                ->constrained('handbook_event_statuses')
                ->cascadeOnDelete();
            $table->string('short_description')
                ->comment('Короткое описание');
            $table->string('description')
                ->comment('Описание');
            $table->string('main_image')
                ->comment('Главное изображение');
            $table->string('additional_image')
                ->nullable()
                ->comment('Дополнительный изображение');
            $table->date('date_start')
                ->comment('Дата начала мероприятия');
            $table->date('date_end')
                ->comment('Дата окончания мероприятия');
            $table->boolean('is_active')
                ->default(1)
                ->comment('Активность');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
