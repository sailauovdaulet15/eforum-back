<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterEventStatementsTable extends Migration
{
    private const TABLE_NAME = 'event_statements';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->text('surname')->nullable()->change();
            $table->renameColumn('position','post');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->string('surname')->nullable(false)->change();
            $table->renameColumn('post','position');
        });
    }
}
