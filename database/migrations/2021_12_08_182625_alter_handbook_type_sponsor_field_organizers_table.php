<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterHandbookTypeSponsorFieldOrganizersTable extends Migration
{
    private const TABLE_NAME = 'organizers';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropConstrainedForeignId('handbook_type_sponsor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->foreignId('handbook_type_sponsor_id')
                ->comment('Тип спонсора')
                ->constrained('handbook_type_sponsors')
                ->cascadeOnDelete();
        });
    }
}
