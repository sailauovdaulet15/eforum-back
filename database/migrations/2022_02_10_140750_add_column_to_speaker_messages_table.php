<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToSpeakerMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('speaker_messages', function (Blueprint $table) {
            $table->boolean('is_active')
                ->default(1)
                ->comment('Активность');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('speaker_messages', function (Blueprint $table) {
            $table->dropColumn('is_active');
        });
    }
}
