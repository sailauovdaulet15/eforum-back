<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterHandbookHallsTable extends Migration
{
    private const TABLE_NAME = 'handbook_halls';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->foreignId('location_city_id')->default(1)
                ->comment('Локация: город')
                ->constrained('location_cities');
            $table->foreignId('handbook_site_id')->default(1)
                ->comment('Справочник: Площадка')
                ->constrained('handbook_sites');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropConstrainedForeignId('location_city_id');
            $table->dropConstrainedForeignId('handbook_site_id');
        });
    }
}
