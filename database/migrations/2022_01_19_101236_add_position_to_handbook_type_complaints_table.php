<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPositionToHandbookTypeComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('handbook_type_complaints', function (Blueprint $table) {
            $table->integer('position')->default(0)->comment('Позиция');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('handbook_type_complaints', function (Blueprint $table) {
            $table->dropColumn('position');
        });
    }
}
