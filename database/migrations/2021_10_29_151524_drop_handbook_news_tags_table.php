<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropHandbookNewsTagsTable extends Migration
{
    private const TABLE_NAME = 'handbook_news_tags';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ref_news_handbook_news_tag', function (Blueprint $table) {
            $table->dropForeign('ref_news_handbook_news_tag_handbook_news_tag_id_foreign');
            $table->dropColumn('handbook_news_tag_id');
        });

        Schema::dropIfExists(self::TABLE_NAME);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Название');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
