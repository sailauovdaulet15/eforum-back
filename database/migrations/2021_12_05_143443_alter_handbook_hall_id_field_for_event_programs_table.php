<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterHandbookHallIdFieldForEventProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_programs', function (Blueprint $table) {
            $table->foreignId('handbook_hall_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_programs', function (Blueprint $table) {
            $table->foreignId('handbook_hall_id')->nullable(false)->change();
        });
    }
}
