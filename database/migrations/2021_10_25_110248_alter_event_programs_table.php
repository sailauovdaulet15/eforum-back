<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterEventProgramsTable extends Migration
{
    private const TABLE_NAME = 'event_programs';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('hall');
            $table->text('description')->nullable()->change();
            $table->foreignId('handbook_hall_id')->comment('Зал')
                ->constrained('handbook_halls')
                ->cascadeOnDelete();
            $table->integer('position')->default(0)->comment('Позиция');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->string('description')->nullable(false)->change();
            $table->dropColumn('position');
            $table->dropConstrainedForeignId('handbook_hall_id');
        });
    }
}
