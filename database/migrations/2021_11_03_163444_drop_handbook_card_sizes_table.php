<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropHandbookCardSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign('events_handbook_card_size_id_foreign');
            $table->dropColumn('handbook_card_size_id');
        });

        Schema::dropIfExists('handbook_card_sizes');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('handbook_card_sizes', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique()->comment('Название карточки');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
