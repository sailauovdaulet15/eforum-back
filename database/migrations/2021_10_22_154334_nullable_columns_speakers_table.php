<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NullableColumnsSpeakersTable extends Migration
{
    private const TABLE_NAME = 'speakers';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->string('photo')->nullable()->change();
            $table->text('about_speaker')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->string('photo')->nullable(false)->change();
            $table->text('about_speaker')->nullable(false)->change();
        });
    }
}
