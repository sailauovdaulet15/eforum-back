<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNullableColumnForHandbookSitesTable extends Migration
{
    private const TABLE_NAME = 'handbook_sites';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->text('address')->comment('Адрес')->nullable()->change();
            $table->float('latitude', 10,6)->comment('Ширина')->nullable()->change();
            $table->float('longitude', 10,6)->comment('Долгота')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->text('address')->comment('Адрес')->nullable(false)->change();
            $table->float('latitude', 10,6)->comment('Ширина')->nullable(false)->change();
            $table->float('longitude', 10,6)->comment('Долгота')->nullable(false)->change();
        });
    }
}
