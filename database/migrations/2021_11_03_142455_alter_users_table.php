<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterUsersTable extends Migration
{
    private const TABLE_NAME = 'users';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->renameColumn('name', 'full_name');
            $table->string('phone')->comment('Номер телефона');
            $table->string('password')->nullable()->comment('Пароль')->change();
            $table->string('company')->comment('Компания');
            $table->string('post')->comment('Должность');
            $table->foreignId('location_country_id')->comment('Страна')
                ->constrained('location_countries')
                ->cascadeOnDelete();
            $table->foreignId('location_city_id')->comment('Город')
                ->constrained('location_cities')
                ->cascadeOnDelete();
            $table->boolean('account_is_active')->default(0)->comment('Актирована учетная запись');
            $table->timestamp('date_time_last_login')->comment('Дата и время последнего входа в систему');
            $table->boolean('is_notification_events')->default(0)->comment('Уведомление о меропритиях');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->renameColumn('full_name', 'name');
            $table->dropColumn('phone');
            $table->string('password')->nullable(false)->comment('Пароль')->change();
            $table->dropColumn('company');
            $table->dropColumn('post');
            $table->dropConstrainedForeignId('location_country_id');
            $table->dropConstrainedForeignId('location_city_id');
            $table->dropColumn('account_is_active');
            $table->dropColumn('date_time_last_login');
            $table->dropColumn('is_notification_events');
        });
    }
}
