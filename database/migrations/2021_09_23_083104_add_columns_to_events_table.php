<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->foreignId('handbook_card_size_id')
                ->comment('Размер карточки')
                ->constrained('handbook_card_sizes')
                ->cascadeOnDelete();
            $table->foreignId('handbook_event_type_id')
                ->comment('Тип мероприятия')
                ->constrained('handbook_event_types')
                ->cascadeOnDelete();
            $table->boolean('payment')
                ->default(1)
                ->comment('Оплата');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropConstrainedForeignId('handbook_event_type_id');
            $table->dropColumn('payment');
        });
    }
}
