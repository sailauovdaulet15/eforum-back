<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixFieldsForBroadcastsTable extends Migration
{
    private const TABLE_NAME = 'broadcasts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->integer('event_id')
                ->nullable()
                ->comment('Мероприятия')
                ->change();
            $table->integer('handbook_hall_id')
                ->nullable()
                ->comment('Зал')
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
