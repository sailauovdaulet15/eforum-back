<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RenameHandbookLocationIdColumnInEventsTable extends Migration
{
    private const TABLE_NAME = 'events';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table(self::TABLE_NAME)->truncate();
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->renameColumn('handbook_location_id', 'handbook_site_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->renameColumn('handbook_site_id', 'handbook_location_id');
        });
    }
}
