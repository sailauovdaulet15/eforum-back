<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_records', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->comment('Мероприятие')
                ->constrained('events')
                ->cascadeOnDelete();
            $table->string('name')->comment('Название записи');
            $table->string('link')->nullable()->comment('Ссылка записи на YouTube');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_records');
    }
}
