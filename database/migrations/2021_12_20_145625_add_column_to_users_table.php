<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('speaker_id')->nullable()
                ->comment('Спикер')
                ->constrained('speakers')
                ->cascadeOnDelete();
            $table->boolean('isSpeaker')->default(0)->comment('Подтвержденный спикер');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropConstrainedForeignId('speaker_id');
            $table->dropColumn('isSpeaker');
        });
    }
}
