<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

class CreateEventCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->comment('Мероприятия')
                ->constrained('events')
                ->cascadeOnDelete();
            $table->string('username')->comment('Имя');
            $table->string('surname')->nullable()->comment('Фамилия');
            $table->string('email')->comment('E-mail');
            $table->text('comment_text')->comment('Текст комментария');
            Nestedset::columns($table);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_comments', function (Blueprint $table){
            NestedSet::dropColumns($table);
        });
        Schema::dropIfExists('event_comments');
    }
}
