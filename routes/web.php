<?php

use App\Http\Controllers\Voyager\VoyagerPositionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Voyager\EventController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/event_date', [EventController::class, 'datePeriodSelect'])
    ->name('event_date_period');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('order_row/{position}/{table}/{sort}/{direction}/{id}', 'App\Http\Controllers\Voyager\VoyagerBreadController@orderRow')
        ->name('order_row');


});
//Route::view('forgot_password', 'auth.reset_password')->name('password.reset');
