<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\DependentDropdownController;
use App\Http\Controllers\Api\ForgotPasswordController;
use App\Http\Controllers\Api\LocationCityController;
use App\Http\Controllers\Api\LocationCountryController;
use App\Http\Controllers\Api\HandbookCategoryController;
use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\HandbookEventKindController;
use App\Http\Controllers\Api\HandbookEventRelevanceController;
use App\Http\Controllers\Api\HandbookEventTypeController;
use App\Http\Controllers\Api\MainPageController;
use App\Http\Controllers\Api\FooterController;
use App\Http\Controllers\Api\MainMenuController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\EventStatementController;
use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\AboutController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\UserEventScheduleController;
use App\Http\Controllers\Api\VerifyUserController;
use App\Http\Controllers\Api\ResetPasswordController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\ComplaintController;
use \App\Http\Controllers\Api\HandbookComplaintController;
use \App\Http\Controllers\Api\BroadcastController;
use \App\Http\Controllers\Api\SpeakerMessagesController;
use App\Http\Controllers\Api\SpeakerController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('main-page', [MainPageController::class, 'index'])->name('main-page');
Route::get('events', [EventController::class, 'index'])->name('events');
Route::get('events-easy', [EventController::class, 'getEasyEvent'])->name('events.easy');
Route::get('footer', [FooterController::class, 'index'])->name('footer');
Route::get('main-menu', [MainMenuController::class, 'index'])->name('main-menu');
Route::get('news', [NewsController::class, 'index'])->name('news');
Route::get('news/{slug}', [NewsController::class, 'slug'])->name('news.slug');
Route::get('comments-user',[CommentController::class, 'getUserComments'])
    ->name('comments.getUserComments');
Route::get('broadcast-user', [BroadcastController::class, 'userList']);
Route::apiResource('comments', CommentController::class)->only('index', 'store');
Route::get('comments/{slug}',[CommentController::class, 'getCommentsByEvent'])->name('comments.getCommentsByEvent');
Route::post('complaints', [ComplaintController::class, 'store'])->name('complaint.store');
Route::apiResource('handbook-complaint', HandbookComplaintController::class)->only('index');
Route::get('contacts', [ContactController::class, 'index'])->name('contact');
Route::get('speaker-messages-list', [SpeakerMessagesController::class, 'getMessages'])
    ->name('speaker.messages');
Route::get('speaker-replies-messages/{id}', [SpeakerMessagesController::class, 'getRepliesMessages'])
    ->name('replies.messages');
Route::get('speaker-event-list/{slug}', [SpeakerController::class, 'getSpeakerByEvent'])->name('speaker.event-list');
Route::apiResource('speaker-messages', SpeakerMessagesController::class)->only('store');
Route::post('event-statements',[EventStatementController::class, 'store'])->name('event-statements.store');
Route::get('event-statements/{id}',[EventStatementController::class, 'show'])->name('event-statements.show');
Route::post('feedback', [FeedbackController::class, 'store'])->name('feedback.store');
Route::get('events/{slug}', [EventController::class, 'view'])->name('events.view');
Route::get('events/{slug}/programs', [EventController::class, 'programs'])->name('events.slug.programs');
Route::get('events/google/schedule', [EventController::class, 'eventSchedule'])->name('events.google.schedule');
Route::get('filter-categories', [HandbookCategoryController::class, 'index'])->name('filter-categories');
Route::get('filter-eventTypes', [HandbookEventTypeController::class, 'index'])->name('filter-eventTypes');
Route::get('filter-relevance', [HandbookEventRelevanceController::class, 'index'])->name('filter-relevance');
Route::get('filter-open', [HandbookEventKindController::class, 'open'])->name('filter.open');
Route::get('filter-close', [HandbookEventKindController::class, 'close'])->name('filter.close');
Route::get('events/{slug}/speakers', [EventController::class, 'speakers'])->name('events.slug.speakers');
Route::get('about', [AboutController::class, 'index'])->name('about');
Route::get('countries', [LocationCountryController::class, 'index'])->name('countries');
Route::get('cities', [LocationCityController::class, 'index'])->name('cities');
Route::post('api/v1/dependent-dropdown', [DependentDropdownController::class, 'index'])->name('api.v1.dropdown');
Route::get('events/{slug}/organizers', [EventController::class, 'organizers'])->name('events.slug.organizers');
Route::group(['middleware' => 'api',], function () {
    Route::post('register', [AuthController::class, 'register'])->name('register');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('refresh-token', [AuthController::class, 'refreshToken'])->name('refresh.token');
    Route::get('profile', [AuthController::class, 'profile'])->name('profile');
    Route::patch('profile-update', [ProfileController::class, 'profileUpdate'])->name('profile.update');
    Route::post('profile/change-avatar', [ProfileController::class, 'changeAvatar']);
    Route::delete('profile/delete-avatar', [ProfileController::class, 'deleteAvatar']);
});
Route::get('user/verify/{token}', [VerifyUserController::class,'verifyUser'])->name('verify.user');
Route::post('forgot-password', [ForgotPasswordController::class, 'forgot'])->name('forgot.password');
Route::post('reset-password/{token}', [ResetPasswordController::class, 'reset'])->name('reset.password');
Route::get('schedule-user-events', [UserEventScheduleController::class, 'schedule'])->name('schedule.user.events')->middleware('auth:api');
